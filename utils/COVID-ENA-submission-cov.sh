#!/usr/bin/sh
###############################################################################
#                       ENA SUBMISSION SCRIPT
#       Reads manifest files from current folder and submit to ENA
#       Webin User and Pass are read from config file
###############################################################################
# Get absolute path of the current script
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $SCRIPT_DIR/../pipeline_parameters.config


echo $WUSER
echo $WPASS

COV=$1

# First we have to check whether a previous submission has been attempted
# and in that case only submit those that have not been successfully submited
java -Xms2048M -Xmx2048M \
  -jar ${WEBIN} \
  -manifest ${COV}.HiQ.ENA_manifest \
  -context reads \
  -submit \
  -username=${WUSER} \
  -password=${WPASS} 

ACC=$(grep "RUN accession" ./reads/${COV}/submit/receipt.xml | cut -f2 -d'"')
echo "${COV},${ACC}" >> COV_ENA.accessions
