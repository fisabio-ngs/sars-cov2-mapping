###############################################################################
## Usage: COVID_runRun.sh <SEQPLACE> <RUNID>
##
## Arguments:
##   <SEQPLACE>        Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>           Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   Run the Illumina MiSeq analysis for the specified run
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 30/09/2021
## __version__   = 1.0.0
##
###############################################################################


###############################################################################
# Show usage and exit
###############################################################################
function showUsage()
{    
    echo -e 'Usage: COVID_runRun.sh <SEQPLACE> <RUNID>'
    echo -e ' Description:'
    echo -e '   Run the Illumina MiSeq analysis for the specified run'
    exit
}


###############################################################################
# Run function
###############################################################################
function run_run()
{
	seq_place=$1
	seq_dir=$1
	run_id=$2

	# Check for special case
	if [ "$seq_place" = "FISABIO" ]; then
		seq_dir='seqfisabio'
	fi

	seqtech='illumina'
	seq='sequencing_data'
	ana='analysis'

	base_folder='/data2/COVID19'
	input_folder=${base_folder}'/'${seq}'/'${seq_dir}'/'${seqtech}'/'${run_id}'/'
    master_table=${base_folder}'/'${seq}'/'${seq_dir}'/'${seqtech}'/'${run_id}'/master_table_'${run_id}'.txt'
	output_folder=${base_folder}'/'${ana}'/'${seq_dir}'/'${seqtech}'/'${run_id}'/'
	num_th=29
	nice_value=4

	echo ''
	echo 'SeqCovid Script Parameters'
	echo '--    Master Table:  ' ${master_table}
	echo '--    Input Folder:  ' ${input_folder}
	echo '--    Output Folder: ' ${output_folder}
	echo '--    Run Id:        ' ${run_id}
	echo '--    Seq Place:     ' ${seq_place}
	echo '--    Num Threads:   ' ${num_th}
	echo ''

	if [ ! -f ${master_table} ]
	then
		echo '>> ERROR: Master Table File does not exist'
		return
	fi

	if [ ! -d ${input_folder} ]
	then
		echo '>> ERROR: Input Folder does not exist'
		return
	fi

	if [ ! -d ${output_folder} ]
	then
		echo '>> ERROR: Output Folder does not exist'
		return
	fi

	echo 'Running SeqCovid Script :)'
	#SeqCOVID-Illumina-pipeline.sh	
	nice -n ${nice_value} \
        SeqCOVID-Illumina-pipeline.sh \
		-m ${master_table}  \
		-f ${input_folder}  \
		-o ${output_folder} \
		-r ${run_id}        \
		-t ${num_th}
}




# Check parameters
if [ $# -ne 2 ]; then
    echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
    showUsage
fi

## Run: Parameters & Execution
run_run $1 $2

## Additional tasks ######################################

# Create the alignment for the consensus
COVID_CreateAlignedConsensus.sh $1 $2


# Backup of analysis in yersin
ssh sjimenez@yersin.ibv.csic.es "nohup ~/mirror_results.sh  > /dev/null 2>&1 &"

