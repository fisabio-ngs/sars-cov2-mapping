#!/bin/bash
###############################################################################
## Usage: COVID_GISAID_extract_ids.sh <tsv_path.tsv>
##
## Arguments:
##   <tsv_path.tsv> Path to a text file containing a list of GISAID EPI codes ids
## 
## Description:
##   Gets a set of files to update the ddbb's given a GISAID EPI codes list.
##   Use as inputs also the tsv_path.tsv GISAID metadata file.
##   As results, generates the next set of files:
##        ./<tsv_path.tsv>.sql             ==> Sql script to update the hscovid database
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 21/10/2021
## __version__   = 3.0.0
##
###############################################################################


function showUsage()
{
    echo -e 'Usage: COVID_GISAID_extract_ids.sh <tsv_path.tsv>'
    echo -e ''
    echo -e 'Arguments:'
    echo -e '\t <tsv_path.tsv> Path to a text file containing a list of GISAID EPI codes ids'
    exit -1
}


function getCOVInfoField()
{
    echo $(cat $1 | grep -F $2 | cut -f2 -d':')
}

# Preconditions

if [ $# -ne 1 ]; then
    echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
    showUsage
fi

if [ ! -f $1 ]; then
    echo -e '\n[ERROR] Input file does not exist: ' $1 'n'
    showUsage
fi


# Global variables
INFILE=$1
COVEPI_LIST='./covepi_list.txt'
CSV_FILE=${INFILE}'.csv'
SQL_FILE=${INFILE}'.sql'
AUTHORSLIST='/data2/COVID19/software/sars-cov2-mapping/Authors_list.csv'



#################################################################
# The file COVEPI_LIST must be generated
#################################################################

# Get the first two columns skipping the header
tail -n +2 ${INFILE} | cut -f1,2 > aux.txt

# Get the code corresponding to the cov identifier from the virus-name
cat aux.txt | cut -f3 -d'/' | cut -f3 -d '-' > covs.txt

# remove the first 2 characters (special code: 97, 98, 99)
echo -n > covsok.txt
for i in $(cat covs.txt); do
    c="${i:2}"
    echo 'COV'$c >> covsok.txt
done

# Put all this info in a single file with 3 columns
paste covsok.txt aux.txt > ${COVEPI_LIST}

# Remove temporal files
rm aux.txt
rm covs.txt
rm covsok.txt

# Output the info
cat ${COVEPI_LIST}


#################################################################




# Create the output files
echo > $CSV_FILE
echo > $SQL_FILE

# Auxiliar file to save the cov.info data
covinfo=cov.info

# Read each line
while read COV VIRUSNAME EPI
do
    echo -e '\t cov=>' $COV '\t epi=>' $EPI '\t virusname=>' $VIRUSNAME
    COVID_getCOVSampleInfo.py -silent 1 -cov $COV > $covinfo

    # Get the real database id, and hospital code
    fid=$(getCOVInfoField $covinfo s.id)
    hos=$(getCOVInfoField $covinfo h.hosp_code)

    # Get the Institurion name and Sample authors
    hhh=$(cat $AUTHORSLIST | grep '('$hos')' | cut -f 2 -d';')
    aut=$(cat $AUTHORSLIST | grep '('$hos')' | cut -f 3 -d';')

    # Get the correct authors value
    aut=$(echo $aut 'and SeqCOVID-SPAIN consortium')

	# Output the csv line
	echo -e $fid';'$COV';'$VIRUSNAME';'$EPI';'$aut >> $CSV_FILE

    fid=($fid)
    # Output the sql instance
    # UPDATE sample SET 
    #    gisaid     = "<value>", 
    #    gisaid_epi = "<value>",
    #    gisaid_authors = "<value>" 
    # WHERE (id = '<value>');
    echo -n -e 'UPDATE sample SET '         >> $SQL_FILE
    echo -n -e ' gisaid="'$VIRUSNAME'", '   >> $SQL_FILE
    echo -n -e ' gisaid_epi="'$EPI'", '     >> $SQL_FILE
    echo -n -e ' gisaid_authors="'$aut'" '  >> $SQL_FILE
    echo -n -e "WHERE (id = '"$fid"');"     >> $SQL_FILE
    echo    -e ''                           >> $SQL_FILE
    
done < $COVEPI_LIST


# Remove Auxiliar files
if [ -f $covinfo ]; then
    rm $covinfo
fi


# Debug
echo -e ''
echo -e '[DONE]'
echo -e 'Output files: '
echo -e '\t =>' $CSV_FILE
echo -e '\t =>' $SQL_FILE
