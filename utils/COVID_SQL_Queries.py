#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utility Functions to perform SQL SELECT queries to the hscovid DB
'''

#---------------------------------------------------------------
__author__      = "Santiago Jiménez-Serrano"
__credits__     = ["Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse
import COVID_Utils             as cutils
import COVID_SpainMap_Utils    as sp_map_utils
import COVID_loadDBCredentials as dbcredentials


##########################################################################
# Arguments parsing
##########################################################################

def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(description='Performs SQL queries to the hscovid DB')
    parser.add_argument("-runid", "--runid", dest="runid", required=False, default='none')
    parser.add_argument("-stats", "--stats", dest="stats", required=False, default='none')
    args = parser.parse_args()

    return args



##########################################################################
# Basic functions
##########################################################################

def print_data(sql_data):
    '''
    Print to the console the specified table
    '''
    for record in sql_data:
        first_column = True
        for o in record:
            if first_column:
                print(o, end='')
                first_column = False
            else:
                print('\t', o, end='')
        print('')

    # Print a final line
    print('')


def print_data_ccaa(sql_data):
    '''
    Print to the console the specified table 
    (1st column is CCAA hospital code prefix)
    '''
    for record in sql_data:
        first_column = True
        for o in record:
            if first_column:
                print(o, end='')
                first_column = False
            else:
                print('\t', o, end='')
        print('')

    # Print a final line
    print('')


def Execute_SELECT(sql_select):
    ''' 
    Connect to the DB, execute the SELECT 
    command and return the result-set
    '''

    # Gets a new connection to the DB
    cnx = dbcredentials.new_connection()

    # Create a cursor
    mycursor = cnx.cursor()

    # Run the SQL sentence
    mycursor.execute(sql_select)

    # Get all the SQL info
    SQL_info = mycursor.fetchall()

    # Close the cursor & the connection
    mycursor.close()
    cnx.close()

    # Return the result set
    return SQL_info



##########################################################################
# Execute SQL SELECT queries
##########################################################################

#=====================
# Microreact queries
#=====================

def Execute_SELECT_microreact_info():
    '''
    Return a result set with the Microreact
    information needed to perform a new submit
    '''
    return Execute_SELECT(get_SELECT_microreact_info())


#=====================
# RunId queries
#=====================

def Execute_SELECT_RunInfo_ReportShort(runid):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_RunInfo_ReportShort(runid))


def Execute_SELECT_RunInfo_ReportLong(runid):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_RunInfo_ReportLong(runid))


def Execute_SELECT_RunInfo_ReportLineages(runid):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_RunInfo_ReportLineages(runid))


#=====================
# Stats queries
#=====================

def Execute_SELECT_NumSamplesInGISAID():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesInGISAID())


def Execute_SELECT_NumSamplesInENA():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesInENA())


def Execute_SELECT_CountStatus():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_CountStatus())


def Execute_SELECT_NumSamplesByHospital_Short():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByHospital_Short())


def Execute_SELECT_NumSamplesByHospital_Long():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByHospital_Long())


def Execute_SELECT_NumSamplesByCCAA():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByCCAA())


def Execute_SELECT_NumSamplesBySeqPlace():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesBySeqPlace())
    

def Execute_SELECT_NumSamplesByLineage():
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_NumSamplesByLineage())


#=====================
# ISCIII Stats queries
#=====================

def Execute_SELECT_ISCIII_Stats(condition):
    '''
    Execute the SQL statement for the main ISCIII stats report
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats(condition))


def Execute_SELECT_ISCIII_Stats_Lineages():
    '''
    Execute the SQL statement for the main ISCIII lineages report
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats_Lineages())


def Execute_SELECT_ISCIII_Stats_Mut_E484K():
    '''
    Execute the SQL statement for the main ISCIII E484K report
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats_Mut_E484K())


#=========================
# ISCIII Stats queries v02
#=========================

def Execute_SELECT_ISCIII_Stats_v02_row(ccaa_prefix, week_start, week_end, condition = None):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats_v02_row(ccaa_prefix, week_start, week_end, condition))


def Execute_SELECT_ISCIII_Stats_v02_lineages(ccaa_prefix, week_start, week_end):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats_v02_lineages(ccaa_prefix, week_start, week_end))


def Execute_SELECT_ISCIII_Stats_v02_mutations(ccaa_prefix, week_start, week_end):
    '''
    <>
    '''
    return Execute_SELECT(get_SELECT_ISCIII_Stats_v02_mutations(ccaa_prefix, week_start, week_end))



##########################################################################
# Get SQL SELECT queries
##########################################################################

#=====================
# Microreact queries
#=====================

def get_SELECT_microreact_info():
    '''
    Return the SQL query with the Microreact
    information needed to perform a new submit
    '''

    return \
        " SELECT " \
	    "    CONCAT('COV', LPAD(s.gId, 6, '0')) AS ID, " + \
        "    s.status, "                                 + \
        "    s.seq_place, "                              + \
        "    s.seq_tech, "                               + \
        "    s.run_code, "                               + \
	    "    s.coverage, "                               + \
	    "    s.median_depth, "                           + \
	    "    s.lineage AS lineage__autocolour, "         + \
	    "    h.name, "                                   + \
	    "    h.latitude AS hosp_latitude, "              + \
	    "    h.longitude AS hosp_longitude, "            + \
	    "    s.hospital_date, "                          + \
	    "    s.gender AS Gender__Autocolor, "            + \
	    "    s.age AS Age, "                             + \
	    "    s.residence_city, "                         + \
        "    s.residence_province, "                     + \
        "    s.residence_ccaa, "                         + \
        "    s.residence_country, "                      + \
        "    s.sample_latitude, "                        + \
        "    s.sample_longitude, "                       + \
	    "    h.address "                                 + \
        " FROM sample s INNER JOIN hospital h  "         + \
        " ON s.hospital_id = h.id "                      + \
        " WHERE  "                                       + \
        "    s.deleted_dt LIKE '' AND "                  + \
	    "    s.status LIKE 'Sequenced%' "                + \
        " ORDER BY " \
        "    s.id "


#=====================
# RunId queries
#=====================

def get_SELECT_RunInfo_ReportShort(runid):
    '''
    <>
    '''

    return \
        "SELECT "                        + \
	    " s.run_code AS RUN_ID, "        + \
	    " s.status   AS STATUS,  "       + \
	    " COUNT(*)   AS NUM_SAMPLES "    + \
        "FROM sample s "                 + \
        "WHERE  "                        + \
	    " s.deleted_dt LIKE ''   AND "   + \
	    " s.run_code LIKE \"" + str(runid) + "\" " + \
        " GROUP BY s.run_code, s.status "  + \
        " ORDER BY s.run_code, s.status"


def get_SELECT_RunInfo_ReportLong(runid):
    '''
    <>
    '''

    return \
        "SELECT "                        + \
	    " s.run_code AS RUN_ID, "        + \
	    " s.status   AS STATUS,  "       + \
	    " h.name     AS HOSPITAL_NAME, " + \
	    " COUNT(*)   AS NUM_SAMPLES "    + \
        "FROM sample s, hospital h "     + \
        "WHERE  "                        + \
	    " s.hospital_id = h.id   AND "   + \
	    " s.deleted_dt LIKE ''   AND "   + \
	    " s.run_code LIKE \"" + str(runid) + "\" " + \
        " GROUP BY s.run_code, s.status, h.name "  + \
        " ORDER BY s.run_code, h.name, s.status"


def get_SELECT_RunInfo_ReportLineages(runid):
    '''
    <>
    '''
    return \
        " SELECT "                  + \
        "  s.lineage, "             + \
        "  s.status, "              + \
        "  s.run_code, "            + \
        "  COUNT(*) AS nsamples  "  + \
        " FROM sample s  "          + \
        " WHERE  "                  + \
        "  s.deleted_dt LIKE '' AND "              + \
        "  s.run_code LIKE '" + str(runid) + "' "  + \
        " GROUP BY s.lineage, s.status, s.run_code " 


#=====================
# Stats queries
#=====================

def get_SELECT_NumSamplesInGISAID():
    '''
    <>
    '''
    
    return \
    "SELECT COUNT(*) "             + \
    "FROM sample s "               + \
    "WHERE s.gisaid NOT LIKE '' "


def get_SELECT_NumSamplesInENA():
    '''
    <>
    '''
    
    return \
    "SELECT COUNT(*) "             + \
    "FROM sample s "               + \
    "WHERE s.ena_accession NOT LIKE '' "


def get_SELECT_CountStatus():
    '''
    <>
    '''

    return \
        " SELECT s.status, COUNT(s.status) AS num " + \
        " FROM sample s"                            + \
        " WHERE s.deleted_dt LIKE ''"               + \
        " GROUP BY s.status " 


def get_SELECT_NumSamplesByHospital_Short():
    '''
    <>
    '''
    return \
        " SELECT "                    + \
        "    h.name, "                + \
        "    h.hosp_code, "           + \
        "    COUNT(*) AS NumSamples " + \
        " FROM sample s "             + \
        " LEFT JOIN hospital h ON (h.id = s.hospital_id AND h.deleted_dt = '') " + \
        " WHERE	    s.deleted_dt = '' " + \
        " GROUP BY	h.name "            + \
        " ORDER BY	h.hosp_code "


def get_SELECT_NumSamplesByHospital_Long():
    '''
    <>
    '''
    return \
        " SELECT "          + \
        "    h.name, "      + \
        "    h.hosp_code, " + \
        "    s.status, "    + \
        "    COUNT(*) "     + \
        " FROM sample s "   + \
        " LEFT JOIN hospital h ON (h.id = s.hospital_id AND h.deleted_dt = '') " + \
        " WHERE	    s.deleted_dt = '' " + \
        " GROUP BY	h.name, s.status "  + \
        " ORDER BY	h.hosp_code, s.status "


def get_SELECT_NumSamplesByCCAA():
    '''
    <>
    '''
    return \
        " SELECT "                      + \
        "    s.residence_ccaa, "        + \
        "    COUNT(*) AS num_samples"   + \
        " FROM sample s "               + \
        " WHERE	    s.deleted_dt = '' " + \
        " GROUP BY	s.residence_ccaa "


def get_SELECT_NumSamplesBySeqPlace():
    '''
    <>
    '''
    return \
        " SELECT  "                       + \
        " s.seq_place, "                  + \
        " COUNT(*) AS num_samples "       + \
        " FROM sample s  "                + \
        " WHERE s.seq_place NOT LIKE '' " + \
        " GROUP BY s.seq_place "          + \
        " ORDER BY num_samples DESC "


def get_SELECT_NumSamplesByLineage():
    '''
    <>
    '''
    return \
        " SELECT "                        + \
        "     s.lineage, "                + \
        "     COUNT(*) AS num_samples "   + \
        " FROM sample s	"                 + \
        " WHERE "                         + \
        "     s.deleted_dt LIKE  '' AND " + \
        "     s.lineage NOT LIKE '' AND " + \
        "     s.lineage NOT LIKE 'None' " + \
        " GROUP BY	s.lineage "


#=====================
# ISCIII Stats queries
#=====================

def get_SELECT_ISCIII_Stats(condition = None):
    '''
    Gets the SQL statement for the main ISCIII stats report
    '''
    sql = \
        " SELECT "                           + \
        "     s.residence_ccaa, "            + \
        "     COUNT(*) AS num_samples "      + \
        " FROM sample s "                    + \
        " WHERE  "                           + \
        " s.deleted_dt LIKE '' AND "         + \
        " s.hospital_date >= '2020-11-30' "

    # Check if condition exist
    if condition:
	    sql += " AND " + str(condition)

    # Append the group by clause
    sql += " GROUP BY s.residence_ccaa "

    return sql


def get_SELECT_ISCIII_Stats_Lineages():
    '''
    Gets the SQL statement for the main ISCIII lineages report
    '''
    return \
        " SELECT "                               + \
        "     s.residence_ccaa, "                + \
        "     s.lineage, "                       + \
        "     COUNT(*) AS num_samples "          + \
        " FROM sample s "                        + \
        " WHERE  "                               + \
        " s.deleted_dt LIKE '' AND "             + \
        " s.hospital_date >= '2020-11-30' AND "  + \
	    " s.coverage >= 0.75 "                   + \
        " GROUP BY s.residence_ccaa, s.lineage "


def get_SELECT_ISCIII_Stats_Mut_E484K():
    '''
    Gets the SQL statement for the main ISCIII E484K report
    '''
    return \
        " SELECT " + \
        "   CONCAT('COV', LPAD(s.id, 6, '0')) AS COVID_ID, " + \
        "   DATE(s.hospital_date), "                         + \
        "   s.residence_province AS RESIDENCE_PROVINCE, "    + \
        "   s.residence_ccaa     AS RESIDENCE_CCAA, "        + \
        "   s.status, "    + \
        "   s.coverage, "  + \
        "   s.lineage, "   + \
        "   s.mutations, " + \
        "   s.clades, "    + \
        "   h.name	 "     + \
        " FROM sample s INNER JOIN hospital h " + \
        " ON s.hospital_id = h.id	 "          + \
        " WHERE  "                              + \
        "   s.deleted_dt LIKE '' AND "          + \
        "   s.mutations LIKE '%484%' AND "      + \
        "   s.hospital_date >= '2020-11-30' "


#=========================
# ISCIII Stats queries v02
#=========================

def get_SELECT_ISCIII_Stats_v02_row(ccaa, week_start, week_end, condition = None):
    '''
    <>
    '''
    sql = \
        " SELECT "                                     + \
        "  s.residence_ccaa, "                         + \
        "  COUNT(*) AS num_samples "                   + \
        " FROM sample s "                              + \
        " WHERE  "                                     + \
        "  s.deleted_dt LIKE '' AND "                  + \
        "  s.residence_ccaa LIKE '" + ccaa + "' AND "  + \
        "  s.hospital_date >= '"+ week_start +"' AND " + \
        "  s.hospital_date <= '"+ week_end +"' AND s.residence_ccaa NOT LIKE 'Braga' AND s.residence_ccaa NOT LIKE 'Porto' "

    # Check if condition exist
    if condition:
	    sql += " AND " + str(condition)

    # Append the group by clause
    sql += " GROUP BY s.residence_ccaa"

    return sql



def get_SELECT_ISCIII_Stats_v02_lineages(ccaa, week_start, week_end):
    '''
    <>
    '''
    sql = \
        " SELECT "                                       + \
        "   s.residence_ccaa, "                          + \
        "   s.lineage, "                                 + \
        "   COUNT(*) AS num_samples "                    + \
        " FROM sample s "                                + \
        " WHERE  "                                       + \
        "  s.deleted_dt LIKE '' AND "                    + \
        "  s.residence_ccaa LIKE '" + ccaa + "' AND "    + \
        "  s.hospital_date >= '"+ week_start +"' AND "   + \
        "  s.hospital_date <= '"+ week_end +"' AND "     + \
        "  s.coverage >= 0.75 AND s.residence_ccaa NOT LIKE 'Braga' AND s.residence_ccaa NOT LIKE 'Porto' "                          + \
        " GROUP BY s.residence_ccaa, s.lineage "

    return sql


def get_SELECT_ISCIII_Stats_v02_mutations(ccaa, week_start, week_end):
    '''
    <>
    '''
    sql = \
        " SELECT "                                      + \
        "    CONCAT('COV', LPAD(s.gId, 6, '0')) AS ID, " + \
        "  DATE(s.hospital_date) as sample_date, "      + \
        "  s.lineage, "                                 + \
        "  s.clades, "                                  + \
        "  s.mutations, "                               + \
        "  s.residence_city, "                          + \
        "  s.residence_province, "                      + \
        "  s.residence_ccaa, "                          + \
        "  s.residence_country "                        + \
        " FROM sample s "                               + \
        " WHERE  "                                      + \
        "  s.deleted_dt LIKE '' AND "                   + \
        "  s.residence_ccaa LIKE '" + ccaa + "' AND "   + \
        "  s.hospital_date >= '" + week_start +"' AND " + \
        "  s.hospital_date <= '" + week_end   +"' AND " + \
        "  s.coverage >= 0.75 AND "                     + \
        "  s.residence_ccaa NOT LIKE 'Braga'  AND "     + \
        "  s.residence_ccaa NOT LIKE 'Porto'  AND "     +\
        "  s.residence_ccaa NOT LIKE 'Maputo' "

    return sql



##########################################################################
# Main
##########################################################################

def main():
    
    args = parse_args()
    runid = args.runid
    stats = args.stats


    # Print runid stats??
    if runid != "none":    
        print_data(Execute_SELECT_RunInfo_ReportShort(runid))
        print_data(Execute_SELECT_RunInfo_ReportLong(runid))
        print_data(Execute_SELECT_RunInfo_ReportLineages(runid))


    # Print report stats??
    if stats != "none":
        print('#samples in GISAID: ')
        print_data(Execute_SELECT_NumSamplesInGISAID())
        print('#samples in ENA: ')
        print_data(Execute_SELECT_NumSamplesInENA())
        print_data(Execute_SELECT_CountStatus())
        print_data(Execute_SELECT_NumSamplesByHospital_Short())
        print_data(Execute_SELECT_NumSamplesByHospital_Long())
        print_data(Execute_SELECT_NumSamplesByCCAA())
        print_data(Execute_SELECT_NumSamplesBySeqPlace())
        print_data(Execute_SELECT_NumSamplesByLineage())

    #print_data(Execute_SELECT_microreact_info())

    return 0


if __name__ == '__main__':
    main()