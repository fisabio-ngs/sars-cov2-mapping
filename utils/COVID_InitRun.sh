###############################################################################
## Usage: COVID_InitRun.sh <SEQPLACE> <RUNID> [SEQTECH] [KOCH_FASTQ_PATH]
##
## Arguments:
##   <SEQPLACE>        Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>           Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
##   [SEQTECH]         Sequencing technology (illumina by default)
##   [KOCH_FASTQ_PATH] Path in koch where the fastq.gz files are placed (copy them)
## 
## Description:
##   Creates the analysis and sequencing_data folders for the given run
##   As results, generates the next set folders:
##     [...]/sequencing_data/<seqplace>/[SEQTECH]/<runid>/
##     [...]/analysis       /<seqplace>/[SEQTECH]/<runid>/
##
##   If the [KOCH_FASTQ_PATH] argument is given, then the fastq.gz files are copied 
##   to the sequencing_folder and the master table is created
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 30/09/2021
## __version__   = 1.0.0
##
###############################################################################


###############################################################################
# Show usage and exit
###############################################################################
function showUsage()
{    
    echo -e 'Usage: COVID_InitRun.sh <SEQPLACE> <RUNID> [SEQTECH] [KOCH_FASTQ_PATH]'
    echo -e ' Description:'
    echo -e '   Creates the analysis and sequencing_data folders for the given run'
    echo -e '   As results, generates the next set folders:'
    echo -e '     [...]/sequencing_data/<seqplace>/[SEQTECH]/<runid>/'
    echo -e '     [...]/analysis       /<seqplace>/[SEQTECH]/<runid>/'
    echo -e ''
    echo -e '   If the [KOCH_FASTQ_PATH] argument is given, then the fastq.gz files are copied '
    echo -e '   to the sequencing_folder and the master table is created'
    exit
}

###############################################################################
# Print success and exit
###############################################################################
function print_success()
{
    echo -e 'COVID_InitRun: [DONE]'
    echo -e '\t Analysis folder:  ' $ana_dir
    echo -e '\t Sequencing folder:' $seq_dir
    exit
}


###############################################################################
# Main instructions
###############################################################################

# Global variables
BASEPATH='/data2/COVID19'
SEQPLACE=null       # Sequencing place
RUNID=null          # Run Id
SEQTECH='illumina'  # Sequencing technology (default: illumina)
KOCH_FASTQ_PATH=''  # Path in koch where the fastq.gz files are placed (copy them)



# Check parameters
if [ $# -eq 2 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
elif [ $# -eq 3 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
elif [ $# -eq 4 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
    KOCH_FASTQ_PATH=$4  # Argument 4
else
    echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
    showUsage
fi


# Check for special case
seqplacedir=$SEQPLACE
if [ "$SEQPLACE" = "FISABIO" ]; then
    seqplacedir='seqfisabio'
fi

# Temporal variables
ana='analysis'
seq='sequencing_data'

# Get the folder paths
ana_dir=${BASEPATH}'/'${ana}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}
seq_dir=${BASEPATH}'/'${seq}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}


# Step 01 - Create the folder path
if [ -d ${ana_dir} ]; then
    echo '[WARNING]: Analysis folder already exists -> ' ${ana_dir}
else
    mkdir ${ana_dir}
    echo '[INFO]: Analysis folder created -> ' ${ana_dir}
fi

if [ -d ${seq_dir} ]; then
    echo '[WARNING]: Sequencig data folder already exists -> ' ${seq_dir}
else
    mkdir ${seq_dir}
    echo '[INFO]: Sequencing data folder created -> ' ${seq_dir}
fi

# Exit if done...
if [ $# -ne 4 ]; then
    print_success
fi


# Step 2 - Copy the files from koch.csic.ibv.es
echo 'Copying the fastq files from koch... '
cd ${seq_dir}
scp koch.ibv.csic.es:${KOCH_FASTQ_PATH}/*.* .
echo 'Copying the fastq files from koch... [DONE]'


# Step 3 - Create the master table (advanced way)
echo 'Creating the master table... '
master_table=${seq_dir}'/master_table_'${RUNID}'.txt'

# Initialize the file (remove previous content if exist)
echo -n > ${master_table}

# For each R1 file...
for FQ1 in $(ls *R1*.fastq.gz)
do

    # Get the fastq2 file path
    FQ2=$(echo "${FQ1}" | sed 's/R1/R2/')

    # Check if FQ2 file exists
    if [ ! -f ${FQ2} ]; then
        echo 'ERROR: FASTQ R2 file does not exist: ' ${FQ2}
    fi
    
    # Better way to get the sample id, removing all the '_' characters
    # Samples of file names:
    # COV006344_S32_L001_R1_001.fastq.gz   (5 tokens) -> COV00634
    # SW_129_MLN1_S90_L001_R2_001.fastq.gz (7 tokens) -> SW129MLN1S90

    # Remove the fixed suffix and the Rep occurrence (if some Rep exist)
    sample=$(echo "${FQ1}" | sed 's/_L001_R1_001.fastq.gz//' | sed 's/Rep//' )
    
    # Get the identifier depending on te prefix
    if [[ ${sample} = COV* ]]; then 
        sample=$(echo "${sample}" | cut -f1 -d'_')
    else
        sample=$(echo "${sample}" | sed 's/_//g')
    fi  

    # Output with the formatted line to the master table
    echo "${sample};${FQ1};${FQ2}" >> ${master_table}

done

# Needed to append a new line for better behaouviour during analysis
echo '' >> ${master_table}

# End info line
echo 'Creating the master table... [DONE] -> ' ${master_table}


# Step 6 - Print success message
print_success

