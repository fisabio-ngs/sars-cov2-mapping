#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utility Functions to manage SeqCOVID main reports
'''
#---------------------------------------------------------------
__author__      = 'Santiago Jiménez-Serrano'
__credits__     = ['Santiago Jiménez-Serrano']
__copyright__   = 'Copyright 2021, SeqCOVID-Spain Consortium'
__email__       = 'bioinfo.covid19@ibv.csic.es'
#---------------------------------------------------------------


#import sys
import argparse
import COVID_Utils          as cutils
import COVID_SpainMap_Utils as sp_map_utils
import COVID_SQL_Queries    as csql



##########################################################################
# Static variables
##########################################################################

isciii_epi_weeks = [ 
    #('2020-Week-45', '2020-11-02', '2020-11-08'),
    #('2020-Week-46', '2020-11-09', '2020-11-15'),
    #('2020-Week-47', '2020-11-16', '2020-11-22'),
    #('2020-Week-48', '2020-11-23', '2020-11-29'),
    ('2020-Week-49', '2020-11-30', '2020-12-06'),   ## los informes se hacen desde esta semana
    ('2020-Week-50', '2020-12-07', '2020-12-13'),   ## line 02
    ('2020-Week-51', '2020-12-14', '2020-12-20'),   ## line 03
    ('2020-Week-52', '2020-12-21', '2020-12-27'),   ## line 04
    ('2020-Week-53', '2020-12-28', '2021-01-03'),   ## line 05
    ('2021-Week-01', '2021-01-04', '2021-01-10'),   ## line 06
    ('2021-Week-02', '2021-01-11', '2021-01-17'),   ## line 07
    ('2021-Week-03', '2021-01-18', '2021-01-24'),   ## line 08
    ('2021-Week-04', '2021-01-25', '2021-01-31'),   ## line 09
    ('2021-Week-05', '2021-02-01', '2021-02-07'),   ## line 10
    ('2021-Week-06', '2021-02-08', '2021-02-14'),   ## line 11
    ('2021-Week-07', '2021-02-15', '2021-02-21'),   ## line 12
    ('2021-Week-08', '2021-02-22', '2021-02-28'),   ## line 13
    ('2021-Week-09', '2021-03-01', '2021-03-07'),   ## line 14
    ('2021-Week-10', '2021-03-08', '2021-03-14'),   ## line 15
    ('2021-Week-11', '2021-03-15', '2021-03-21'),   ## line 16
    ('2021-Week-12', '2021-03-22', '2021-03-28'),   ## line 17
    ('2021-Week-13', '2021-03-29', '2021-04-04'),   ## line 18
    ('2021-Week-14', '2021-04-05', '2021-04-11'),   ## line 19
    ('2021-Week-15', '2021-04-12', '2021-04-18'),   ## line 20
    ('2021-Week-16', '2021-04-19', '2021-04-25'),   ## line 21
    ('2021-Week-17', '2021-04-26', '2021-05-02'), 
    ('2021-Week-18', '2021-05-03', '2021-05-09'), 
    ('2021-Week-19', '2021-05-10', '2021-05-16'), 
    ('2021-Week-20', '2021-05-17', '2021-05-23'), 
    ('2021-Week-21', '2021-05-24', '2021-05-30'), 
    ('2021-Week-22', '2021-05-31', '2021-06-06'), 
    ('2021-Week-23', '2021-06-07', '2021-06-13'), 
    ('2021-Week-24', '2021-06-14', '2021-06-20'), 
    ('2021-Week-25', '2021-06-21', '2021-06-27'), 
    ('2021-Week-26', '2021-06-28', '2021-07-04'), 
    ('2021-Week-27', '2021-07-05', '2021-07-11'), 
    ('2021-Week-28', '2021-07-12', '2021-07-18'), 
    ('2021-Week-29', '2021-07-19', '2021-07-25'), 
    ('2021-Week-30', '2021-07-26', '2021-08-01'), 
    ('2021-Week-31', '2021-08-02', '2021-08-08'), 
    ('2021-Week-32', '2021-08-09', '2021-08-15'), 
    ('2021-Week-33', '2021-08-16', '2021-08-22'), 
    ('2021-Week-34', '2021-08-23', '2021-08-29'), 
    ('2021-Week-35', '2021-08-30', '2021-09-05'), 
    ('2021-Week-36', '2021-09-06', '2021-09-12'), 
    ('2021-Week-37', '2021-09-13', '2021-09-19'), 
    ('2021-Week-38', '2021-09-20', '2021-09-26'), 
    ('2021-Week-39', '2021-09-27', '2021-10-03'), 
    ('2021-Week-40', '2021-10-04', '2021-10-10'), 
    ('2021-Week-41', '2021-10-11', '2021-10-17'), 
    ('2021-Week-42', '2021-10-18', '2021-10-24'), 
    ('2021-Week-43', '2021-10-25', '2021-10-31'), 
    ('2021-Week-44', '2021-11-01', '2021-11-07'), 
    ('2021-Week-45', '2021-11-08', '2021-11-14'), 
    ('2021-Week-46', '2021-11-15', '2021-11-21'), 
    ('2021-Week-47', '2021-11-22', '2021-11-28'), 
    ('2021-Week-48', '2021-11-29', '2021-12-05'), 
    ('2021-Week-49', '2021-12-06', '2021-12-12'), 
    ('2021-Week-50', '2021-12-13', '2021-12-19'), 
    ('2021-Week-51', '2021-12-20', '2021-12-26'), 
    ('2021-Week-52', '2021-12-27', '2022-01-02')
]

default_mutations = {
    'A.23.1'    : ['F157L_S',    'V367F_S',    'Q613H_S', 'P681R_S'], 
    'A.27'      : ['L18F_S',     'L452R_S',    'N501Y_S', 'D796Y_S',  'G1219V_S', 'H655Y_S', 'A653V_S'], 
    'A.28'      : ['69-70del_S', 'N501T',      'H655Y_S'], 
    'B.1.1.318' : ['T95I_S',     '144del_S',   'E484K_S', 'D614G_S',  'P681H_S',  'D796H_S'], 
    'B.1.1.7'   : ['69-70del_S', '144del_S',   'N501Y_S', 'A570D_S',  'D614G_S',  'P681H_S', 'T716I_S', 'S982A_S', 'D1118H_S'], 
    'B.1.324.1' : ['E484K_S',    'S494P',      'N501Y_S', 'D614G_S',  'P681H_S',  'E1111K'], 
    'B.1.351'   : ['D80A',       'D215G',      'K417N_S', 'E484K_S',  'N501Y_S',  'D614G_S', 'A701V_S'], 
    'B.1.427'   : ['S13I_S',     'W152C_S',    'L452R_S', 'D614G_S'], 
    'B.1.429'   : ['S13I_S',     'W152C_S',    'L452R_S', 'D614G_S'], 
    'B.1.525'   : ['Q52R_S',     '69-70del_S', '144del_S', 'E484K_S', 'D614G_S',  'Q677H_S', 'F888L_S'],
    'B.1.526'   : ['T95I_S',     'D253G_S',    'D614G_S'], 
    'B.1.526.1' : ['D80G',       '144del_S',   'F157L_S', 'L452R_S',  'D614G_S',  'D950H'], 
    'B.1.575'   : [], 
    'B.1.575.1' : [], 
    'B.1.617'   : ['L452R_S',    'E484Q',      'D614G_S', 'P681R_S'], 
    'B.1.617.1' : ['E154K',      'L452R_S',    'E484Q',   'D614G_S',  'P681R_S',  'Q1017H'], 
    'B.1.617.2' : ['T19R',       'del157/158', 'L452R_S', 'T478K',    'D614G_S',  'P681R_S', 'D950N'], 
    'B.1.617.3' : ['T19R',       'L452R_S',    'E484Q',   'D614G_S',  'P681R_S',  'D950N'], 
    'B.1.620'   : ['S477N',      'E484K_S',    'D614G_S'], 
    'B.1.621'   : ['R346K',      'E484K_S',    'N501Y_S', 'D614G_S'], 
    'C.37'      : [], 
    'P.1'       : ['L18F_S',     'T20N_S',     'P26S_S',  'D138Y_S',  'R190S_S',  'K417T_S', 'E484K_S', 'N501Y_S', 'D614G_S', 'H655Y_S', 'T1027I_S'], 
    'P.2'       : ['E484K_S',    'D614G_S'], 
    'P.3'       : ['E484K_S',    'N501Y_S',    'P681H_S', 'D614G_S',  'E1092K_S', 'H1101Y_S', 'V1176F_S']
}


##########################################################################
# Arguments parsing
##########################################################################

def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(
        description='Outputs the basic report stats according to the hscovid DB')
    parser.add_argument("-isciii",   "--isciii",   dest="isciii",   required=False, default='none')
    parser.add_argument("-isciiiv2", "--isciiiv2", dest="isciiiv2", required=False, default='none')
    args = parser.parse_args()

    return args


##########################################################################
# Report functions
##########################################################################

def print_iscii_report():
    '''
    <>
    '''
    print_iscii_report_Summary()
    print_iscii_report_Lineages()
    print_iscii_report_E484K()
    

def print_iscii_report_Summary():
    '''
    <>
    '''
    print('## Muestras enviadas')
    csql.print_data_ccaa(csql.Execute_SELECT_ISCIII_Stats(None))

    print('## Calidad insuficiente para secuenciación (coverage < 0.75)')
    csql.print_data_ccaa(csql.Execute_SELECT_ISCIII_Stats('s.coverage < 0.75'))

    print('## Secuenciada completa (coverage >= 0.90)')
    csql.print_data_ccaa(csql.Execute_SELECT_ISCIII_Stats('s.coverage >= 0.90'))

    print('## Secuencia incompleta pero suficiente para asignar linajes. (coverage [0.75, 0.90[)')
    csql.print_data_ccaa(csql.Execute_SELECT_ISCIII_Stats('s.coverage >= 0.75 AND s.coverage < 0.90'))


def print_iscii_report_Lineages():
    '''
    <>
    '''
    lineages_table = csql.Execute_SELECT_ISCIII_Stats_Lineages()
    print_iscii_report_Lineages_ByCCAA(lineages_table)
    print_iscii_report_Lineages_All(lineages_table)


def print_dict(dict, header = ''):
    '''
    <>
    '''
    print(header)
    for key in dict:
        print(key, '\t', dict[key])
    print()


def append2dict(thedict, ccaa, nsamples):
    '''
    '''
    if ccaa not in thedict:
        thedict[ccaa] = nsamples
    else:
        thedict[ccaa] += nsamples
    return thedict


def print_iscii_report_Lineages_ByCCAA(lineages_table):
    '''
    <>
    '''
    
    # Init the Dictionaries
    dict_b_1_1_7  = {}
    dict_b_1_351  = {}
    dict_b_1_1_28 = {}
    dict_b_1_177  = {}
    dict_b_1_5    = {}
    dict_b_1_1    = {}
    dict_b_1_258  = {}
    dict_others   = {}

    # Fill the Dictionaries
    for row in lineages_table:
        ccaa     = str(row[0])
        lineage  = str(row[1])
        nsamples = int(row[2])

        # Fill the B.1.1.7 dict
        if lineage == 'B.1.1.7' or lineage.startswith('B.1.1.7.'):
            dict_b_1_1_7 = append2dict(dict_b_1_1_7, ccaa, nsamples)

        # Fill the B.1.351 dict
        elif lineage == 'B.1.351' or lineage.startswith('B.1.351.'):
            dict_b_1_351 = append2dict(dict_b_1_351, ccaa, nsamples)

        # Fill the B.1.1.28 dict
        elif lineage == 'B.1.1.28' or lineage.startswith('B.1.1.28.'):
            dict_b_1_1_28 = append2dict(dict_b_1_1_28, ccaa, nsamples)

        # Fill the B.1.177 dict
        elif lineage == 'B.1.177' or lineage.startswith('B.1.177.'):
            dict_b_1_177 = append2dict(dict_b_1_177, ccaa, nsamples)

        # Fill the B.1.5 dict
        elif lineage == 'B.1.5' or lineage.startswith('B.1.5.'):
            dict_b_1_5 = append2dict(dict_b_1_5, ccaa, nsamples)

        # Fill the B.1.1 dict
        elif lineage == 'B.1.1' or lineage.startswith('B.1.1.'):
            dict_b_1_1 = append2dict(dict_b_1_1, ccaa, nsamples)

        # Fill the B.1.258 dict
        elif lineage == 'B.1.258' or lineage.startswith('B.1.258.'):
            dict_b_1_258 = append2dict(dict_b_1_258, ccaa, nsamples)

        # Fill the others dict
        else:
            dict_others = append2dict(dict_others, ccaa, nsamples)
            

    # Print the Dictionaries
    print_dict(dict_b_1_1_7,  'Lineage B.1.1.7  (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_351,  'Lineage B.1.351  (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_1_28, 'Lineage B.1.1.28 (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_177,  'Lineage B.1.177  (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_5,    'Lineage B.1.5    (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_1,    'Lineage B.1.1    (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_b_1_258,  'Lineage B.1.258  (coverage >= 0.75) [30/11/2020 - hoy]')
    print_dict(dict_others,   'Other lineages   (coverage >= 0.75) [30/11/2020 - hoy]')


def print_iscii_report_Lineages_All(lineages_table):
    '''
    <>
    '''
    print('## Tabla detallada de linajes por CCAA (coverage >= 0.75) [30/11/2020 - hoy]')
    csql.print_data_ccaa(lineages_table)


def print_iscii_report_E484K():
    '''
    <>
    '''
    print('## Muestras que contienen la mutación E484K [30/11/2020 - hoy]')
    csql.print_data(csql.Execute_SELECT_ISCIII_Stats_Mut_E484K())
    


##########################################################################
# Report functions v02
##########################################################################

def get_first_row_value(sql_result):
    '''
    <>
    '''
    value = 0
    n = 0
    for row in sql_result:
        value += row[1]
        n = n + 1
        #if n > 1:
        #    print("Error: More than one row in query!!!")
    return value


def get_printable_value(value):
    '''
    <>
    '''
    if value <= 0:
        return ''
    return value


def is_lineage(lineage, lin_str):
    '''
    <>
    '''
    return lineage == lin_str or lineage.startswith(lin_str + '.')


def get_lineages_counts(lineages_table):
    '''
    <>
    '''

    # Init the Dictionaries    
    num_a_27       = 0
    num_a_28       = 0
    num_b_1_1_318  = 0
    num_b_1_1_7    = 0
    num_b_1_177    = 0    
    num_b_1_351    = 0
    num_b_1_427    = 0
    num_b_1_429    = 0
    num_b_1_525    = 0
    num_b_1_526    = 0
    num_c_37       = 0
    num_p_1        = 0
    num_p_2        = 0
    num_p_3        = 0
    num_b_1_575    = 0
    num_b_1_617_1  = 0
    num_b_1_617_2  = 0
    num_b_1_617_3  = 0
    num_b_1_621    = 0

    num_others     = 0
    other_lin      = {}

    # Fill the Dictionaries
    for row in lineages_table:
        ccaa     = str(row[0])
        lineage  = str(row[1])
        nsamples = int(row[2])

        
        if is_lineage(lineage, 'A.27'):
            num_a_27 += nsamples

        elif is_lineage(lineage, 'A.28'):
            num_a_28 += nsamples

        elif is_lineage(lineage, 'B.1.1.318'):
            num_b_1_1_318 += nsamples
        
        elif is_lineage(lineage, 'B.1.1.7'):
            num_b_1_1_7 += nsamples
        
        elif is_lineage(lineage, 'B.1.177'):
            num_b_1_177 += nsamples

        elif is_lineage(lineage, 'B.1.351'):
            num_b_1_351 += nsamples

        elif is_lineage(lineage, 'B.1.427'):
            num_b_1_427 += nsamples

        elif is_lineage(lineage, 'B.1.429'):
            num_b_1_429 += nsamples

        elif is_lineage(lineage, 'B.1.525'):
            num_b_1_525 += nsamples

        elif is_lineage(lineage, 'B.1.526'):
            num_b_1_526 += nsamples

        elif is_lineage(lineage, 'C.37'):
            num_c_37 += nsamples

        elif is_lineage(lineage, 'P.1'):
            num_p_1 += nsamples

        elif is_lineage(lineage, 'P.2'):
            num_p_2 += nsamples

        elif is_lineage(lineage, 'P.3'):
            num_p_3 += nsamples

        elif is_lineage(lineage, 'B.1.575'):
            num_b_1_575 += nsamples

        elif is_lineage(lineage, 'B.1.617.1'):
            num_b_1_617_1 += nsamples

        elif is_lineage(lineage, 'B.1.617.2'):
            num_b_1_617_2 += nsamples

        elif is_lineage(lineage, 'B.1.617.3'):
            num_b_1_617_3 += nsamples

        elif is_lineage(lineage, 'B.1.621'):
            num_b_1_621 += nsamples

        # Fill the others dict
        else:
            num_others += nsamples
            if lineage in other_lin:
                other_lin[lineage] = other_lin[lineage] + nsamples
            else: 
                other_lin[lineage] = nsamples

    # return a tuple with the values
    return (num_a_27,      \
            num_a_28,      \
            num_b_1_1_318, \
            num_b_1_1_7,   \
            num_b_1_177,   \
            num_b_1_351,   \
            num_b_1_427,   \
            num_b_1_429,   \
            num_b_1_525,   \
            num_b_1_526,   \
            num_c_37,      \
            num_p_1,       \
            num_p_2,       \
            num_p_3,       \
            num_b_1_575,   \
            num_b_1_617_1, \
            num_b_1_617_2, \
            num_b_1_617_3, \
            num_b_1_621,   \
            num_others, other_lin)


def parse_mutations(mut):
    '''
    Return the mutations in the string that has a frequency greater than 0.8
    '''

    if not mut or str(mut) == 'NA':
        return []

    list = str(mut).split(sep='&')
    rlist = []
    for m in list:
        values   = m.split('(')
        mutation = values[0]
        freq     = values[1].replace(')', '')
        if ('NA' in freq or float(freq) >= 0.8) and (mutation.endswith('_S')):
            rlist.append(mutation)

    # Clean mutations list
    return rlist



def count_mutations(query_mutations):
    '''
    <>
    '''

    lineages = {}

    for row in query_mutations:
        cov  = row[0]
        dt   = row[1]
        lin  = row[2]
        cla  = row[3]
        mut  = row[4]
        rci  = row[5]
        rpr  = row[6]
        rca  = row[7]
        rco  = row[8]

        if not lin in lineages:
            lineages[lin] = {}

        mlist = parse_mutations(mut)

        for m in mlist:
            if not m in lineages[lin]:
                lineages[lin][m] = 1
            else:
                lineages[lin][m] = lineages[lin][m] + 1
        
    #print (lineages) 
    return lineages

def do_print_mutation(lineage, mutation):
    '''
    <>
    '''
    if not lineage in default_mutations:
        return False
    
    list_muts = default_mutations[lineage]
    for m in list_muts:
        if m == mutation:
            return False

    # The mutation is not in the default lineage mutations; it must be printed
    return True

def get_mutations_str(lineages_dict):
    '''
    <>
    '''

    s = ''
    first = True
    for lineage in lineages_dict:
        for mutation in lineages_dict[lineage]:
            if (do_print_mutation(lineage, mutation)):
                num_samples = lineages_dict[lineage][mutation]
                if not first:
                    s += ', '
                first = False
                mm = str(mutation).replace('_S', '')
                s += lineage + ' + ' + mm + '(' + str(num_samples) + ')'
    return s


def print_iscii_report_v02_row(ccaa, week_number, week_start, week_end):
    '''
    <>
    '''

    # Query the database
    received  = csql.Execute_SELECT_ISCIII_Stats_v02_row(ccaa, week_start, week_end, None)
    QA_low    = csql.Execute_SELECT_ISCIII_Stats_v02_row(ccaa, week_start, week_end, 's.coverage < 0.75 ')
    QA_high   = csql.Execute_SELECT_ISCIII_Stats_v02_row(ccaa, week_start, week_end, 's.coverage >= 0.90 ')
    QA_enough = csql.Execute_SELECT_ISCIII_Stats_v02_row(ccaa, week_start, week_end, 's.coverage >= 0.75 AND s.coverage < 0.90 ')

    # Get the first row value (number)
    nreceived  = get_first_row_value(received)
    nqa_low    = get_first_row_value(QA_low)
    nqa_high   = get_first_row_value(QA_high)
    nqa_enough = get_first_row_value(QA_enough)

    # Query the database for the number of samples and lineages for a given ccaa and a range of dates
    query_lineages = csql.Execute_SELECT_ISCIII_Stats_v02_lineages(ccaa, week_start, week_end)

    # Query the database for all the rows to parse the mutations for a given ccaa and a range of dates
    query_mutations = csql.Execute_SELECT_ISCIII_Stats_v02_mutations(ccaa, week_start, week_end)
    

    # Count the samples by lineage
    lineages_tuple = get_lineages_counts(query_lineages)

    # Get the number of samples by lineage by ccaa    
    num_a_27       = get_printable_value(lineages_tuple[ 0])
    num_a_28       = get_printable_value(lineages_tuple[ 1])
    num_b_1_1_318  = get_printable_value(lineages_tuple[ 2])
    num_b_1_1_7    = get_printable_value(lineages_tuple[ 3])
    num_b_1_177    = get_printable_value(lineages_tuple[ 4])    
    num_b_1_351    = get_printable_value(lineages_tuple[ 5])
    num_b_1_427    = get_printable_value(lineages_tuple[ 6])
    num_b_1_429    = get_printable_value(lineages_tuple[ 7])
    num_b_1_525    = get_printable_value(lineages_tuple[ 8])
    num_b_1_526    = get_printable_value(lineages_tuple[ 9])
    num_c_37       = get_printable_value(lineages_tuple[10])
    num_p_1        = get_printable_value(lineages_tuple[11])
    num_p_2        = get_printable_value(lineages_tuple[12])
    num_p_3        = get_printable_value(lineages_tuple[13])
    num_b_1_575    = get_printable_value(lineages_tuple[14])
    num_b_1_617_1  = get_printable_value(lineages_tuple[15])
    num_b_1_617_2  = get_printable_value(lineages_tuple[16])
    num_b_1_617_3  = get_printable_value(lineages_tuple[17])
    num_b_1_621    = get_printable_value(lineages_tuple[18])
    num_others     = get_printable_value(lineages_tuple[19])
    other_lin      = lineages_tuple[20]

    # Get the number of total samples by ccaa
    nreceived  = get_printable_value(nreceived)
    nqa_low    = get_printable_value(nqa_low)
    nqa_high   = get_printable_value(nqa_high)
    nqa_enough = get_printable_value(nqa_enough)


    # String for other lineages
    i = 0
    n = len(other_lin)
    olstr = ''
    for l in other_lin:
        olstr += l + ' (' + str(other_lin[l]) + ')'
        if i < n-1 and n > 1:
            olstr += ', '
        i += 1

    # Get the mutations string
    dict_lineages_mutations = count_mutations(query_mutations)
    str_mutations = get_mutations_str(dict_lineages_mutations)

    
    # Print the resulting line
    print(week_number,     '\t', 
            nreceived,     '\t',
            nqa_low,       '\t',
            nqa_high,      '\t',
            nqa_enough,    '\t',            
            num_a_27,      '\t',
            num_a_28,      '\t',
            num_b_1_1_318, '\t',
            num_b_1_1_7,   '\t',
            num_b_1_177,   '\t',            
            num_b_1_351,   '\t',
            num_b_1_427,   '\t',
            num_b_1_429,   '\t',
            num_b_1_525,   '\t',
            num_b_1_526,   '\t',
            num_c_37,      '\t',
            num_p_1,       '\t',
            num_p_2,       '\t',
            num_p_3,       '\t',
            num_b_1_575,   '\t',
            num_b_1_617_1, '\t',
            num_b_1_617_2, '\t',
            num_b_1_617_3, '\t',
            num_b_1_621,   '\t',
            num_others,    '\t', 
            olstr,         '\t',
            str_mutations)

    #csql.print_data(query_mutations)

    

def print_iscii_report_v02_ccaa(ccaa):

    print(ccaa)

    # Change the number in order to get more weeks    
    for w in range(34):

        week_number = isciii_epi_weeks[w][0]
        week_start  = isciii_epi_weeks[w][1]
        week_end    = isciii_epi_weeks[w][2]
        #print(week_number, '-', week_start, '-', week_end)
        print_iscii_report_v02_row(ccaa, week_number, week_start, week_end)
    
    # Print final line
    print()


    


def print_iscii_report_v02():
    '''
    <>
    '''

    for ccaa in sp_map_utils.CCAA_spain_db:        
        print_iscii_report_v02_ccaa(ccaa)

    #print('Spain')
    #print_iscii_report_v02_ccaa('%')

    # Total Spain
    # print('Informe final')
    #print_iscii_report_v02_ccaa('SPAIN', '%')






##########################################################################
# Main
##########################################################################

def main():
    '''
    Main entry point
    '''
    
    # Parse args
    args = parse_args()
    isciii   = args.isciii
    isciiiv2 = args.isciiiv2    

    # Print runid stats??
    if isciii != "none":
        print_iscii_report()

    # Print runid stats??
    if isciiiv2 != "none":
        print_iscii_report_v02()


    # Return EXIT_SUCCESS
    return 0


if __name__ == '__main__':
    main()






