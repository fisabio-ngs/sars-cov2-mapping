###############################################################################
## Usage: COVID_StartRun.sh <SEQPLACE> <RUNID>
##
## Arguments:
##   <SEQPLACE>        Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>           Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   Run the Illumina MiSeq analysis for the specified run in a nohup process
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 30/09/2021
## __version__   = 1.0.0
##
###############################################################################


###############################################################################
# Show usage and exit
###############################################################################
function showUsage()
{    
    echo -e 'Usage: COVID_StartRun.sh <SEQPLACE> <RUNID>'
    echo -e ' Description:'
    echo -e '   Run the Illumina MiSeq analysis for the specified run in a nohup process'
    exit
}


# Check parameters
if [ $# -ne 2 ]; then
    echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
    showUsage
fi

###############################################################################
# Main instructions
###############################################################################

# Global variables
BASEPATH='/data2/COVID19'
SEQPLACE=$1         # Sequencing place
RUNID=$2            # Run Id
SEQTECH='illumina'  # Sequencing technology (default: illumina)


# Check for special case
seqplacedir=$SEQPLACE
if [ "$SEQPLACE" = "FISABIO" ]; then
    seqplacedir='seqfisabio'
fi

# Temporal variables
ana='analysis'
seq='sequencing_data'

# Get the folder paths
ana_dir=${BASEPATH}'/'${ana}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}
seq_dir=${BASEPATH}'/'${seq}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}


# Change to the seq_dir in order to save there the nohup.out file
if [ ! -d ${seq_dir} ]; then
    echo -e '\n[ERROR] Input data folder does not exist: ' ${seq_dir} '\n'
    showUsage
fi
cd ${seq_dir}

## Star the Run in a nohup process
nohup COVID_runRun.sh $1 $2 &
echo '[INFO] Run started in a nohup independent process... ;)'

