#! /bin/bash

for FQ in $(ls *.fastq.gz)
do   
  sample=$(echo ${FQ} | cut -f1 -d'_' | cut -f1 -d'.' )
  echo ${sample}';'${FQ}
done
echo ''