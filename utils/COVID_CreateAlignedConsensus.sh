#!/bin/bash
###############################################################################
## Usage (1): COVID_CreateAlignedConsensus.sh <SEQPLACE> <RUNID>
## Usage (2): COVID_CreateAlignedConsensus.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   (1). Create the aligned consensus versus the reference for the given SEQPLACE and RUNID
##   (2). Create the aligned consensus versus the reference for all SEQPLACEs and RUNIDs
##
##   As results, generates the next set of files:
##     [...]/analysis/[...]/consensus/<COV>.aln.fas => Aligned fasta
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 02/09/2020
## __version__   = 2.0.0
##
###############################################################################

function showUsage()
{
    echo -e 'Usage (1): create-runid-aligned-consensus.sh <SEQPLACE> <RUNID> [SEQTECH]'
    echo -e 'Description: Create the aligned consensus versus the reference for the given SEQPLACE and RUNID'
    echo -e 'Sample: create-consensus.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): create-runid-aligned-consensus -all'
    echo -e 'Description: Create the aligned consensus versus the reference for all SEQPLACEs and RUNIDs'
    echo -e ''	
    exit -1
}

function doAlignment()
{
    echo -e '\t Run_id: \t ' ${RUNID} ' \t\t ' ${path02}

    # For each COV in the run
    for path03 in $(ls $path02/consensus/*.fa)
    do
        # Ensure is File
        if [ ! -f ${path03} ]; then
            continue
        fi

        # Get the COV identifier from the filename
        cov=$(basename ${path03} | cut -f 1 -d'.')

        # For Debug Purposes
        echo -e '\t\t COV: \t ' ${cov} '\t' ${path03}

        faux=${path02}'/consensus/'${cov}'.aux.fas'
        faln=${path02}'/consensus/'${cov}'.aln.fas'
        fmsk=${path02}'/consensus/'${cov}'.aln.masked.fas'

        # Unmasked alignment #####################################################
        # Align respect the reference
        mafft --thread 1 --quiet --keeplength --add $path03 $REFERENCE_ALN > $faux

        # Remove the reference from the fax file
        samtools faidx $faux $cov > $faln

        # Remove auxiliar files
        rm $faux
        rm $faux'.fai'
        ##########################################################################

        # Masked alignment #######################################################
        # Copy the unmasked file to the new masked file
        cp $faln $fmsk

        # first we edit in place to mask the first and last 30 characters
        # and any 7bp window with >=2 uninformative characters
        python '/data/Alvaro/COVID/sarscov2phylo/scripts/mask_seq.py' $fmsk $fmsk
        
        # next we mask the dodgy sites specifically
        seqmagick mogrify --mask $MASK_SITES $fmsk
        ##########################################################################
        
    done # end cov
}



function doAlignment_Folder()
{
    # Illumina
    if [ -d $path01'/illumina' ]; then

        for RUNID in $(ls $path01'/illumina')
        do
            path02=$path01'/illumina/'$RUNID

            # Ensure directory
            if [ ! -d $path02 ]; then
                continue
            fi
            
            # For each COV in the run
            doAlignment

        done # end runid
    fi

    # IonTorrent
    if [ -d $path01'/IonTorrent' ]; then

        for RUNID in $(ls $path01'/IonTorrent')
        do
            path02=$path01'/IonTorrent/'$RUNID

            # Ensure directory
            if [ ! -d $path02 ]; then
                continue
            fi
            
            # For each COV in the run
            doAlignment

        done # end runid
    fi
}





# Global variables
SEQPLACE=null  # Sequencing place
RUNID=null     # Run Id
FLAG_ALL=1     # 1=YES, 0=NO
VERBOSE=1      # 1=YES, 0=NO
BASEPATH='/data2/COVID19/analysis'
REFERENCE_ALN='/data2/COVID19/reference_data/Sars-Cov-2_REFERENCE.fasta'
SEQTECH='illumina'


if [ $# -eq 2 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ $# -eq 3 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi



##########################################################################
# Necessary to perform the fasta masking
##########################################################################
# See: https://github.com/roblanf/sarscov2phylo/blob/master/scripts/mask_alignment.sh

# download latest masking file and extract the sites to mask
if [ -f ./problematic_sites_sarsCov2.mask.vcf ]; then
    rm ./problematic_sites_sarsCov2.mask.vcf
fi
wget https://raw.githubusercontent.com/W-L/ProblematicSites_SARS-CoV2/master/subset_vcf/problematic_sites_sarsCov2.mask.vcf
mask_sites=$(/usr/bin/bcftools query -f '%POS\t' problematic_sites_sarsCov2.mask.vcf)

echo "Masking any 7bp window with >=2 uninformative characters, and the first and last 30bp of every sequence"
echo "These sites tend to have a lot more errors than other sites"
echo "Also masking all sites suggested here: https://raw.githubusercontent.com/W-L/ProblematicSites_SARS-CoV2/master/subset_vcf/problematic_sites_sarsCov2.mask.vcf"

# build the mask sites list for seqmagick
m=""
for site in $mask_sites; do m="$m,$site:$site"; done
m="${m:1}" # cut the first comma

export MASK_SITES=$m
###########################################################################



###########################################################################
# Perform only one reanalysis for the given seqplace & run id
###########################################################################
if [ $FLAG_ALL -eq 0 ]; then
    path01=${BASEPATH}'/'$SEQPLACE
    path02=${path01}'/'${SEQTECH}'/'$RUNID

    # Ensure directory
    if [ ! -d $path02 ]; then
        echo -e 'ERROR: Analysis folder does not exist: ' $path02
        showUsage
    fi

    # Do the aligned consensus
    doAlignment
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi
###########################################################################



###########################################################################
# From here, it is assumed the -all flag ##################################
###########################################################################

# For each sequencing place folder
for SEQPLACE in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}/$SEQPLACE

    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    if [ "$SEQPLACE" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$SEQPLACE

    # FISABIO seq place exception
    if [ "$SEQPLACE" = "seqfisabio" ]; then
        real_seq_place="FISABIO"
    fi
    
    # Some Debug    
    echo -e 'Sequencing Place:\t' $real_seq_place '   \t\t' $path01

    # For each run folder
    doAlignment_Folder

done # end seqplace
