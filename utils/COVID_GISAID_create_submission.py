#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Write metadata files for GISAID, ENA and Microreat 
data uploading within the SeqCOVID project
'''
#---------------------------------------------------------------
__author__      = 'Galo A. Goig, Santiago Jiménez-Serrano'
__credits__     = ['Galo A. Goig', 'Santiago Jiménez-Serrano']
__copyright__   = 'Copyright 2020, SeqCOVID-Spain Consortium'
__email__       = 'bioinfo.covid19@ibv.csic.es'
#---------------------------------------------------------------


import os
import sys
import argparse
import COVID_Utils          as cutils
import COVID_SpainMap_Utils as sp_map_utils
import COVID_SQL_Queries    as dbqueries


def parse_args():
    '''
    Parse arguments given to script
    '''    

    parser = argparse.ArgumentParser(description='Write metadata files for'
    ' GISAID, ENA and Microreat data upload within the SeqCOVID project')

    parser.add_argument('-m', '--master_table',     dest='master_table', metavar='MASTER TABLE', required=True)
    parser.add_argument('-o', '--output-directory', dest='odir', required=True)
    parser.add_argument('--run-id', dest='runid', required=True)
    parser.add_argument("-silent", dest="silent", 
        type=cutils.str2bool, 
        nargs='?', const=True, default=False, help="Specify silent mode")
    args = parser.parse_args()

    return args



def parse_authors_list():    

    # Get path of the running script & replace utils by 'Authors_list.csv'
    utils       = os.path.dirname(os.path.realpath(__file__))
    author_list = utils.replace('utils', 'Authors_list.csv')
    AUTHORS     = {}    
    with open(author_list) as infile:
        infile.readline() # Skip header
        for line in infile:
            line = line.rstrip().split(';')
            hosp_DB, institution, authors = line
            AUTHORS[hosp_DB] = (institution, authors)

    return AUTHORS


def Load_COVS(args):
    '''
    Loads the list of COVS to retrieve the info from the run master_table
    '''    

    mtable_path = os.path.abspath(args.master_table)
    mtable_dir  = os.path.dirname(mtable_path)
    covs = {}

    with open(args.master_table) as infile:        
        for line in infile:
            cov    = line.split(';')[0].rstrip('m') # m just in case is resequencing
            fastq1 = '{}/{}.R1.humanfilt.fastq.gz'.format(mtable_dir, cov)
            fastq2 = '{}/{}.R2.humanfilt.fastq.gz'.format(mtable_dir, cov)

            # Try avoiding non COV samples like negatives
            if cov.startswith('COV'): 
                covs[cov] = (fastq1, fastq2)
    return covs


def parse_SQL_info(args, SQL_info):
    '''
    Given the data returned by the SQL query, parse it, clean it
    and store into a dict
    '''

    '''
    # COLUMN  0 => sample.gId
    # COLUMN  1 => sample.hospital_date
    # COLUMN  2 => sample.residence_city
    # COLUMN  3 => sample.residence_province
    # COLUMN  4 => sample.residence_country
    # COLUMN  5 => sample.gender
    # COLUMN  6 => sample.age
    # COLUMN  7 => sample.seq_tech
    # COLUMN  8 => hospital.name
    # COLUMN  9 => hospital.address
    # COLUMN 10 => sample.lineage
    # COLUMN 11 => sample.coverage
    # COLUMN 12 => sample.median_depth
    # COLUMN 13 => hospital.latitude
    # COLUMN 14 => hospital.longitude
    # COLUMN 15 => sample.sip
    # COLUMN 16 => sample.sample_latitude
    # COLUMN 17 => sample.sample_longitude
    # COLUMN 18 => sample.hospital_sample_number
    '''

    COV_info = {}
    for record in SQL_info:        
        cov          = cutils.number2cov(record[0])        
        dt, yy, mm, \
            dd, epiw = cutils.getDetailedDate(record[1], 'unknown')
        city         = record[2].strip().replace(' ', '_')
        province     = record[3].strip().replace(' ', '_')
        country      = record[4].strip().replace(' ', '_')
        gender       = cutils.getSafeGender(record[5])
        age          = cutils.getSafeAge(   record[6])
        seq_tech     = record[7].replace(' ', '_')
        ori_lab      = record[8].strip().replace(' ', '_')
        ori_address  = record[9].strip()
        lineage      = record[10].strip()
        branch       = cutils.getTreeBranch(lineage)
        coverage     = record[11]
        median_depth = record[12]
        latitude     = record[13]
        longitude    = record[14]
        patient      = record[15]
        sample_latitude  = record[16]
        sample_longitude = record[17]
        hospital_sample_number= record[18]
        seq_place    = record[19]

        # Get the sample origin Province & City from the Hospital Address
        ori_PROV = ori_address.split(',')[-2].strip().replace(' ', '_')
        ori_CITY = ori_address.split(',')[-3].strip().replace(' ', '_')
        ori_ccaa = sp_map_utils.get_CCAA(ori_PROV)

        # Si no se conoce la ciudad, provincia, pais,
        # se le pone la del hospital mas una _h para saberlo
        if not city:
            city = '{}_h'.format(ori_CITY)

        # Ensure some value for the province
        if not province or not sp_map_utils.exist_spain_province(province):
            province = ori_PROV

        # Set always Spain as origin country
        if country != 'Spain':
            if not args.silent:
                WARN = 'Warning [{}]: => '.format(cov)        \
                    + 'Origin country is {} '.format(country) \
                    + 'but it has been set to default (Spain)\n'
                sys.stderr.write(WARN)
            country = 'Spain'
            
        
        # Get the full CCAA name
        ccaa = sp_map_utils.get_CCAA(province)

        # Get the patient residence location
        location = 'Europe/{}/{}/{}'.format(country, ccaa, city)

        # Get the origin location (hospital)
        hospital_location = 'Europe/Spain/{}/{}'.format(ori_ccaa, ori_CITY)

        # Get the COVxxxxxx number, (6-zero padded)
        cov_number = str(record[0]).rjust(6, '0')

        # Hospital's Province ==> CCAA code
        ccaa_code  = sp_map_utils.get_CCAA_ISO3166(ori_PROV)

        # Seq Place GISAID code        
        gcode = cutils.gisaid_originlab_code(seq_place)

        # The virus-name's Identifier field (GISAID)
        identifier = ccaa_code + '-IBV-' + gcode + cov_number

        # Set to 2020 the year if unknown (GISAID)
        gyear = yy
        if yy == 'unknown':
            gyear = '2020'

        # Set Virus Name according to GISAID standards
        #  hCoV-19/Country/Identifier/year of collection
        virus_name = 'hCoV-19/Spain/{}/{}'.format(identifier, gyear)

        
        # Create the record in the output dictionary
        COV_info[cov] = {
            'hosp_date'         : dt,
            'year'              : yy,
            'month'             : mm,
            'day'               : dd,
            'city'              : city,
            'province'          : province,
            'country'           : country,
            'location'          : location,
            'hospital_location' : hospital_location,
            'gender'            : gender,
            'age'               : age,
            'seq_tech'          : seq_tech,
            'seq_place'         : seq_place,
            'ori_lab'           : ori_lab,
            'ori_address'       : ori_address,
            'lineage'           : lineage,
            'branch'            : branch,
            'coverage'          : coverage,
            'median_depth'      : median_depth,
            'virus_name'        : virus_name,
            'latitude'          : latitude,
            'longitude'         : longitude,
            'patient'           : patient,
            'sample_latitude'   : sample_latitude,
            'sample_longitude'  : sample_longitude,
            'hospital_sample_number' : hospital_sample_number
            }

    return COV_info


def SQL_query(args, covs):
    ''' 
    Connect to the DB, retrieve info for a given list of COVS
    and return that info into a dictionary
    '''

    # quito el COV y lo paso a int para quitar los 0s de delante ya que los ids
    # en la BD internamente están como numeros
    ids = tuple(map(lambda x: int(str.replace(x,'COV','')),covs))

    # In case there is only one cov, it must be a string instead of a
    # tuple for the SQL query
    comparison_operator = 'IN '
    if len(ids) == 1:
        ids = ids[0]
        comparison_operator = '= '


    # Get the SQL sentence
    sql = \
        'SELECT '                   + \
        's.gId, '                   + \
        's.hospital_date, '         + \
        's.residence_city, '        + \
        's.residence_province, '    + \
        's.residence_country, '     + \
        's.gender, '                + \
        's.age, '                   + \
        's.seq_tech, '              + \
        'h.name, '                  + \
        'h.address, '               + \
        's.lineage, '               + \
        's.coverage, '              + \
        's.median_depth, '          + \
        'h.latitude, '              + \
        'h.longitude, '             + \
        's.sip, '                   + \
        's.sample_latitude, '       + \
        's.sample_longitude, '      + \
        's.hospital_sample_number, ' + \
        's.seq_place ' + \
        'FROM sample s INNER JOIN hospital h ' + \
        ' ON s.hospital_id = h.id' + \
        ' WHERE s.gId ' + comparison_operator + str(ids)
                     

    # Run the sql query
    SQL_info = dbqueries.Execute_SELECT(sql)
    
    # Returns the results set
    return SQL_info



def write_GISAID_metadata(args, COV_info):
    '''
    Function to generate the row with the info to submit to GISAID
    '''
    
    # Path is to get absolute and normalized PATH for the output directory
    # LOAD DB info for provided covs
    odir = os.path.abspath(args.odir)

    # If output directory does not exists, create it
    if not os.path.exists(odir):
        os.mkdir(odir)

    runid = args.runid
    AUTHORS = parse_authors_list()
    header_GISAID =          \
        'Submitter@'       + \
        'FASTA filename@'  + \
        'Virus name@'      + \
        'Type@'            + \
        'Passage details/history@'+\
        'Collection date@' + \
        'Location@'        + \
        'Additional location information@' + \
        'Host@'            + \
        'Additional host information@' + \
        'Gender@'          + \
        'Patient age@'     + \
        'Patient status@'  + \
        'Specimen source@' + \
        'Outbreak@'        + \
        'Last vaccinated@' + \
        'Treatment@'       + \
        'Sequencing technology@' + \
        'Assembly method@' + \
        'Coverage@'        + \
        'Originating lab@' + \
        'Address@'         + \
        'Sample ID given by the sample provider@' + \
        'Submitting lab@'  + \
        'Address@'         + \
        'Sample ID given by the submitting laboratory@' + \
        'Authors@'         + \
        'Comment@'         + \
        'Comment Icon\n'

    with open('{}/GISAID_{}_metadata.csv'.format(odir, runid), 'w') as outfile:

        # El archivo de GISAID se llamara, e.g: GISAID_200514_MO01239_metadata.csv
        # donde args.runid == 200514_MO01239
        outfile.write(header_GISAID)

        for cov in COV_info:
            hosp_DB = COV_info[cov]['ori_lab']
            if hosp_DB in AUTHORS:
                institution, authors = AUTHORS[hosp_DB]
            else:
                institution = hosp_DB
                authors = 'AUTHORS'
            
            # Append the SeqCOVID suffix to authors
            authors = '{} and SeqCOVID-SPAIN consortium'.format(authors)
            address        = 'Jaume Roig St, 11. Valencia. E-46010. Spain'
            submitting_lab = 'SeqCOVID-SPAIN consortium/IBV(CSIC)'
            fasta_filename = 'GISAID_consensus.{}.fasta'.format(runid)

            # In GISAID, the sample location must be the hospital one
            location               = COV_info[cov]['hospital_location']
            additional_location    = 'Patient residence: {}'.format(COV_info[cov]['location'])
            hospital_sample_number = COV_info[cov]['hospital_sample_number']

            GISAID_row = '{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}@{}\n'.format(
            'seqcovid',                  # Submitter
            fasta_filename,              # Fasta filename
            COV_info[cov]['virus_name'], # Virus name
            'betacoronavirus',           # Type
            'Original',                  # Passage details/history
            COV_info[cov]['hosp_date'],  # Collection date
            location,                    # Location
            additional_location,         # Additional_location_info
            'Human',                     # Host
            '',                          # Additional_host_info
            COV_info[cov]['gender'],     # Gender
            COV_info[cov]['age'],        # Age
            'unknown',                   # Patient status
            '',                          # Specimen source
            '',                          # Outbreak
            '',                          # Last_vaccinated
            '',                          # Treatment,
            COV_info[cov]['seq_tech'],   # Sequencing technology
            'BWA/IVAR',                  # Assembly_method
            COV_info[cov]['median_depth'],  # mean_depth, GISAID says 'coverage'
            institution,                 # Originating laboratory
            COV_info[cov]['ori_address'],# Ori Lab Address
            hospital_sample_number,      # Sample ID from provider
            submitting_lab,              # Submitting Lab
            address,                     # Address
            cov,                         # Sample ID from submitter
            authors,                     # Authors
            '',                          # Comments
            ''                           # Icon
            )

            # Only write for genomes that pass QC
            #if qc_dict[cov]['coverage'] >= 0.9:
            outfile.write(GISAID_row)
        return 0





def main():
    
    # Load arguments
    args = parse_args()

    # Read the master table
    covs = Load_COVS(args)

    # Read the DB
    SQL_info = SQL_query(args, covs)

    # Pass SQL_info to a dictionary
    COV_info = parse_SQL_info(args, SQL_info)
    
    # Write the metadata files
    write_GISAID_metadata(args, COV_info)

    return 0



if __name__ == '__main__':
    main()