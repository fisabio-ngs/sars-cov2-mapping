#!/bin/bash
###############################################################################
## Usage (1): COVID-update-clades.sh <SEQPLACE> <RUNID>
## Usage (2): COVID-update-clades.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
## 
## Description:
##   (1). Gets the SARS-CoV-2 genomes typing for the given SEQPLACE and RUNID
##   (2). Gets the SARS-CoV-2 genomes typing for all the SEQPLACEs and RUNIDs
##
##   As results, generates the next file:
##     [...]/analysis/[...]/variants/Typing_Report.${RUNID}.txt
##   
##   If -all flag is enabled, it also generates the next file:
##     ALL_Typing_Reports.txt => Contains all the results for all the runs
##
##
## __author__    = "Santiago Jiménez-Serrano & Álvaro Chiner-Oms" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 25/09/2020
## __version__   = 2.0.0
##
###############################################################################



function showUsage()
{
    echo -e 'Usage (1): COVID-update-clades.sh <SEQPLACE> <RUNID>'
    echo -e 'Description: Gets the SARS-CoV-2 genomes typing for the given SEQPLACE and RUNID'
    echo -e 'Sample: COVID-update-clades.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): COVID-update-clades.sh -all'
    echo -e 'Description: Gets the SARS-CoV-2 genomes typing for all the SEQPLACEs and RUNIDs'
    echo -e ''	
    exit -1
}


function write_header()
{
    echo -e -n 'ID\t'           >  $1
    echo -e -n 'SEQPLACE\t'     >> $1
    echo -e -n 'RUNID\t'        >> $1
    echo -e -n 'DB_RUNID\t'     >> $1
    echo -e -n 'Status\t'       >> $1
    echo -e -n 'Coverage\t'     >> $1
    echo -e -n 'Lineage\t'      >> $1
    echo -e -n 'Clades\t'       >> $1
    echo -e -n 'Mutations\t'    >> $1
    echo -e -n 'HospitalDate\t' >> $1
    echo -e -n 'HospitalCode\t' >> $1
    echo -e -n 'Province\t'     >> $1
    echo -e -n 'CCAA_code\t'    >> $1
    echo -e -n 'CCAA_name\n'    >> $1
}


function getCOVInfoField()
{
    echo $(cat $1 | grep -F $2 | cut -f2 -d':')
}


function doAnalysis()
{
    # Debug
    echo -e '\t SARS-CoV-2 genomes typing => \t SeqPlace: '$SEQPLACE'\t RunId: '$RUNID

    # Check for FISABIO seq place
    if [ "${SEQPLACE}" = "FISABIO" ]; then
    	SEQPLACE='seqfisabio'
    fi

    # Get the analysis folder
    workfolder=${BASEPATH}'/'${SEQPLACE}'/'${SEQTECH}'/'${RUNID}

    # Check that the working folder exist
    if [ ! -d ${workfolder} ]; then
        echo -e '[ERROR] Analysis Folder does not exist -> ' ${workfolder}
        return
    fi


    # Create the output file paths
    variantsfolder=${workfolder}'/variants'
    FOUT01=${variantsfolder}'/Typing_Report.'${RUNID}'.txt'

    # Get the auxiliar file paths (dbupdate & pangoling)
    dbupdate_f=${workfolder}'/metadata/DB_UPDATE_'${RUNID}'.csv'
    pangolin_f=${workfolder}'/pangolin/lineages.'${RUNID}'.csv'

    # Write the file headers
    write_header $FOUT01

    lsfilter=${variantsfolder}'/COV*.tsv'
    if [ "${SEQTECH}" = "nanopore" ]; then
        lsfilter=${variantsfolder}'/COV*.pass.vcf'
        lsfilter=${workfolder}'/articNcovNanopore_Genotyping_typeVariants/variants/COV*.variants.csv'
    fi
    

    # For each COVxxxxxx tsv file
    for i in $(ls ${lsfilter})
    do

        # Get the cov
        cov=$(basename $i | cut -f1 -d'.')

        # Get the status for this sample
        status=$(cat ${dbupdate_f} | grep ${cov} | cut -f2 -d',' )

        # Get the coverage for this sample
        coverage=$(cat ${dbupdate_f} | grep ${cov} | cut -f3 -d',' )

        # Get the lineage for this sample
        lineage=$(cat ${pangolin_f} | grep ${cov} | cut -f2 -d',' )

        # Build the input full paths
        path_tsv=$i        

        # Check tsv is a file
        if [ ! -f $path_tsv ]; then
            echo '[WARNING]: ['$cov'] TSV not found => '$path_tsv
            continue
        fi

        # Get the typing and mutations
        ${TYPING_SCRIPT} -v ${path_tsv} > ${COVINFO}

        type=$(cat ${COVINFO} | cut -f2)
        muts=$(cat ${COVINFO} | cut -f3)

        # Get the cov info from the DB
        ${GETCOVINFO_SCRIPT} -silent 1 -cov ${cov} > ${COVINFO}

        # Get the cov DB info related
        f1=$(getCOVInfoField ${COVINFO} s.hospital_date)
        f2=$(getCOVInfoField ${COVINFO} h.hosp_code)
        f3=$(getCOVInfoField ${COVINFO} s.residence_province)        
        f4=$(getCOVInfoField ${COVINFO} s.ccaa_code)
        f5=$(getCOVInfoField ${COVINFO} s.ccaa_name)
        f6=$(getCOVInfoField ${COVINFO} s.run_code)

        # If the ccaa is empty, set the hospital one
        if [ -z $f4 ]; then
            f3=$(getCOVInfoField ${COVINFO} h.ori_PROV)        
            f4=$(getCOVInfoField ${COVINFO} h.ccaa_code)
            f5=$(getCOVInfoField ${COVINFO} h.ccaa_name)
        fi

        # Remove the cov info auxiliar file
        rm ${COVINFO}

        # Debug?
        if [ $VERBOSE -eq 1 ]; then
            echo -e '\t\t COV:' $cov '\tType =' $type '\tMuts = ' $muts 'CCAA =' $f4
        fi

        # Ensure valid seqplace
        splace=$SEQPLACE
        if [ "$splace" = "seqfisabio" ]; then
            splace='FISABIO'
        fi
        
        # 1 - Output the results to the output File
        echo -e $cov'\t'$splace'\t'$RUNID'\t'$f6'\t'$status'\t'$coverage'\t'$lineage'\t' \
            $type'\t'$muts'\t'$f1'\t'$f2'\t'$f3'\t'$f4'\t'$f5 >> $FOUT01

    done

    # Debug
    if [ $FLAG_ALL -eq 0 ]; then
        echo -e '\t[DONE] Results saved to -> '
        echo -e '\t ' ${FOUT01}
        echo -e ''
    fi
}


function doAnalysis_Folder_Tech()
{
    ffpath=$path01'/'${SEQTECH}

    if [ -d ${ffpath} ]; then

        # For each run folder
        for runid in $(ls ${ffpath})
        do
            path02=${ffpath}'/'$runid

            # Ensure directory
            if [ ! -d $path02 ]; then
                continue
            fi

            # Set the global variables and perform the analysis
            SEQPLACE=$real_seq_place  # Sequencing place
            RUNID=$runid
            doAnalysis

            # Write to the total output files jumping the header        
            tail -n +2 ${FOUT01} >> ${OUTFILE01}
            
        done # end runid
    fi
}


function doAnalysis_Folder()
{
    # Illumina
    SEQTECH='illumina'
    doAnalysis_Folder_Tech

    # IonTorrent
    SEQTECH='IonTorrent'
    #doAnalysis_Folder_Tech
    # Para iontorrent no tenemos los ficheros de entrada

    # nanopore
    SEQTECH='nanopore'
    doAnalysis_Folder_Tech
    # Para nanopore habrá que ver si tenemos los ficheros de entrada. En principio alvaro ha dejado el script preparado para ello
}




# Global variables #########################################################
SEQPLACE=null  # Sequencing place
SEQTECH='illumina' # Sequencing technology
RUNID=null     # Run Id
FLAG_ALL=1     # 1=YES, 0=NO
VERBOSE=1      # 1=YES, 0=NO
BASEPATH='/data2/COVID19/analysis'
SWPATH='/data2/COVID19/software/sars-cov2-mapping/utils/'
TYPING_SCRIPT=${SWPATH}'/COVID_clade_typing.py'
GETCOVINFO_SCRIPT=${SWPATH}'/COVID_getCOVSampleInfo.py'
COVINFO='./cov.info'
OUTFILE01='./ALL_Typing_Reports.txt'


# Preconditions ############################################################

if [ $# -eq 2 ];then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ $# -eq 3 ];then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi


#############################################################################
# Perform only one analysis for the given seqplace & run id ?
#############################################################################
if [ $FLAG_ALL -eq 0 ]; then
    doAnalysis
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi



###########################################################################
# From here, it is assumed the -all flag
###########################################################################

# Reset the out file
write_header $OUTFILE01

# For each sequencing place folder
for seq_place in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}/$seq_place

    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    # Jump general_results
    if [ "$seq_place" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$seq_place

    # FISABIO seq place exception
    if [ "$seq_place" = "seqfisabio" ]; then
        real_seq_place="FISABIO"
    fi    

    # For each run folder
    doAnalysis_Folder

done # end seqplace


# Debug
echo -e '[FINAL DONE] Total results saved to -> '
echo -e '\t ' ${OUTFILE01}
echo -e ''

