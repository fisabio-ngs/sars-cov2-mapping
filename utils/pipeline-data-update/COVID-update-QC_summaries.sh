#!/bin/bash
###############################################################################
## Usage (1): COVID-update-QC_summaries.sh <SEQPLACE> <RUNID>
## Usage (2): COVID-update-QC_summaries.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
##   -all       Run the script for all the SEQPLACEs and RUNIDs in the pipeline filesystem
## 
## Description:
##   (1). Updates the QC_Summary for the given SEQPLACE and RUNID
##   (2). Updates the QC_Summary files for all SEQPLACEs and RUNIDs
##
##   As results, generates the next set of files:
##     [...]/analysis/[...]/reports/QC_summary.${RUNID}.txt     => Only COV samples
##     [...]/analysis/[...]/reports/QC_summary.${RUNID}.all.txt => All samples for the run
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 18/11/2020
## __version__   = 1.0.0
##
###############################################################################


###########################################################################
# Show usage and exit
###########################################################################
function showUsage()
{
	echo -e 'Usage (1): COVID-update-QC_summaries.sh <SEQPLACE> <RUNID>'
    echo -e 'Description: Updates the QC_Summary for the given SEQPLACE and RUNID'
    echo -e 'Sample: COVID-update-QC_summaries.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): COVID-update-QC_summaries.sh -all'
    echo -e 'Description: Updates the QC_Summary files for all SEQPLACEs and RUNIDs'
    echo -e ''	
	exit -1
}


###########################################################################
# UpdateQC function for a given SEQPLACE & RUNID
###########################################################################
function updateQC()
{
    # Debug
    echo -e '\tQC_Summary updating => \t SeqPlace: '$SEQPLACE'\t RunId: '$RUNID

    # Precondtions #############################################################

    # Check for FISABIO seq place
    if [ "${SEQPLACE}" = "FISABIO" ]; then
    	SEQPLACE='seqfisabio'
    fi

    # Get the working folder (analysis)
    workfolder=${BASEPATH}/${SEQPLACE}'/illumina/'${RUNID}

    # Check that the working folder exist
    if [ ! -d ${workfolder} ]; then
        echo -e '[ERROR] Analysis Folder does not exist -> ' ${workfolder}
        return
    fi

    #############################################################################

    # Output path only for the COV samples
    QC01=${workfolder}'/reports/QC_summary.'${RUNID}'.csv'

    # Output path for all the samples in the run (including the no COV)
    QC02=${workfolder}'/reports/QC_summary.'${RUNID}'.all.csv'

    # Input master table path
    MASTER_TABLE='/data2/COVID19/sequencing_data/'${SEQPLACE}'/illumina/'${RUNID}'/master_table_'${RUNID}'.txt'

    # Update the QC Summary ###################
    ${SCRIPTS_DIR}'/COVID-analysis-summary.py'\
		 -m ${MASTER_TABLE}                   \
		 --run-id ${RUNID}                    \
		 --run-directory ${workfolder}        \
         -o ${QC01}
    ###########################################

    # Update the QC Summary (all samples) #####
    ${SCRIPTS_DIR}'/COVID-analysis-summary.py'\
		 -m ${MASTER_TABLE}                   \
		 --run-id ${RUNID}                    \
		 --run-directory ${workfolder}        \
		 -all yes                             \
         -o ${QC02}
    ###########################################

    # Debug
    if [ ${VERBOSE} -eq 1 ]; then
        echo -e '\t[DONE] Results saved to -> '
        echo -e '\t ' ${QC01}
        echo -e '\t ' ${QC02}
        echo -e ''
    fi
}


###########################################################################
# Global variables definition
###########################################################################
SEQPLACE='null' # Sequencing place
RUNID='null'    # Run Id
FLAG_ALL=1      # 1=YES, 0=NO
VERBOSE=1       # 1=YES, 0=NO
BASEPATH='/data2/COVID19/analysis'
SCRIPTS_DIR='/data2/COVID19/software/sars-cov2-mapping/utils'


###########################################################################
# Preconditions
###########################################################################
if [ $# -eq 2 ];then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi
###########################################################################


###########################################################################
# Update the QC files only for the given seqplace & run_id
###########################################################################
if [ $FLAG_ALL -eq 0 ]; then
    updateQC
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi
###########################################################################


###########################################################################
# From here, it is assumed the -all flag 
###########################################################################

# For each sequencing place folder
for seq_place in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}'/'$seq_place

    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    # Jump general_results
    if [ "$seq_place" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$seq_place

    # FISABIO seq place exception
    if [ "$seq_place" = "seqfisabio" ]; then
        real_seq_place='FISABIO'
    fi    

    # For each run folder
    for runid in $(ls $path01/illumina)
    do
        path02=$path01'/illumina/'$runid

        # Ensure directory
        if [ ! -d $path02 ]; then
            continue
        fi

        # Set the global variables and perform the analysis
        SEQPLACE=$real_seq_place  # Sequencing place
        RUNID=$runid
        updateQC
        
    done # end runid

done # end seqplace


# Debug
echo -e 'All QC_summaries updating... [DONE]'
echo -e ''