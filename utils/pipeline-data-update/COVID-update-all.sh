#!/bin/bash

# Call all the update scripts for all the seqplaces & runids

./COVID-update-QC_summaries.sh  -all
./COVID-update-QC_summaries_LONG.sh  -all
./COVID-update-metadata.sh  -all
./COVID-update-clades.sh -all


