#!/bin/bash
###############################################################################
## Usage (1): COVID-update-metadata.sh <SEQPLACE> <RUNID>
## Usage (2): COVID-update-metadata.sh -all
##
## Arguments:
##   <SEQPLACE> Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>    Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
##   -all       Run the script for all the SEQPLACEs and RUNIDs in the pipeline filesystem
## 
## Description:
##   (1). Updates the metadata for the given SEQPLACE and RUNID
##   (2). Updates the metadata files for all SEQPLACEs and RUNIDs
##
##   As results, generates the next set of files:
##     [...]/analysis/[...]/reports/DB_UPDATE.${RUNID}.csv     => Only COV samples
##     [...]/analysis/[...]/reports/ ...
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 19/11/2020
## __version__   = 1.0.0
##
###############################################################################


###########################################################################
# Show usage and exit
###########################################################################
function showUsage()
{
	echo -e 'Usage (1): COVID-update-metadata.sh <SEQPLACE> <RUNID>'
    echo -e 'Description: Updates the metadata for the given SEQPLACE and RUNID'
    echo -e 'Sample: COVID-update-metadata.sh FISABIO 200824_M02956'
    echo -e ''
    echo -e 'Usage (2): COVID-update-metadata.sh -all'
    echo -e 'Description: Updates the metadata files for all SEQPLACEs and RUNIDs'
    echo -e ''	
	exit -1
}


###########################################################################
# UpdateMetadata function for a given SEQPLACE & RUNID
###########################################################################
function updateMetadata()
{
    # Debug
    echo -e '\tMetadata updating => \t SeqPlace: '$SEQPLACE'\t RunId: '$RUNID

    # Precondtions #############################################################

    # Check for FISABIO seq place
    if [ "${SEQPLACE}" = "FISABIO" ]; then
    	SEQPLACE='seqfisabio'
    fi

    # Get the working folder (analysis)
    workfolder=${BASEPATH}'/'${SEQPLACE}'/'${SEQTECH}'/'${RUNID}
    metafolder=${workfolder}'/metadata'

    # Check that the folders exist
    if [ ! -d ${workfolder} ]; then
        echo -e '[ERROR] Analysis Folder does not exist -> ' ${workfolder}
        return
    fi

    if [ ! -d ${metafolder} ]; then
        echo -e '[ERROR] Analysis Metadata Folder does not exist -> ' ${metafolder}
        return
    fi

    #############################################################################

    # Input master table path
    MASTER_TABLE='/data2/COVID19/sequencing_data/'${SEQPLACE}'/'${SEQTECH}'/'${RUNID}'/master_table_'${RUNID}'.txt'
    SCRIPTPY=${SCRIPTS_DIR}'/COVID-analysis-metadata.py'
    if [ "${SEQTECH}" = "nanopore" ]; then
    	MASTER_TABLE='/data2/COVID19/sequencing_data/'${SEQPLACE}'/'${SEQTECH}'/'${RUNID}'/SampleSheet_'${RUNID}'.csv'
        SCRIPTPY=${SCRIPTS_DIR}'/COVID-analysis-metadata-nanopore.py'
    fi


    # Update the metadata ######################    
    ${SCRIPTPY}                        \
		 -m ${MASTER_TABLE}            \
		 --run-id ${RUNID}             \
		 --run-directory ${workfolder} \
         -o ${metafolder}
    ############################################

    # Debug
    if [ ${VERBOSE} -eq 1 ]; then
        echo -e '\tMetadata Updating [DONE] '
        echo -e ''
    fi
}

function doAnalysis_Folder_Tech()
{
    # Ensure directory
    if [ ! -d $path01 ]; then
        continue
    fi

    # Jump general_results
    if [ "$seq_place" = "general_results" ]; then
        continue
    fi

    # Get a new variable with the Real Sequencing Place
    real_seq_place=$seq_place

    # FISABIO seq place exception
    if [ "$seq_place" = "seqfisabio" ]; then
        real_seq_place='FISABIO'
    fi    

    ffpath=${path01}'/'${SEQTECH}

    if [ -d ${ffpath} ]; then

        # For each run folder
        for runid in $(ls ${ffpath})
        do
            path02=${ffpath}'/'${runid}

            # Ensure directory
            if [ ! -d $path02 ]; then
                continue
            fi

            # Set the global variables and perform the analysis
            SEQPLACE=$real_seq_place  # Sequencing place
            RUNID=$runid
            updateMetadata
            
        done # end runid

    fi
}

function doAnalysis_Folder()
{
    # Illumina
    SEQTECH='illumina'
    doAnalysis_Folder_Tech

    # IonTorrent
    #SEQTECH='IonTorrent'
    #doAnalysis_Folder_Tech

    # IonTorrent
    SEQTECH='nanopore'
    doAnalysis_Folder_Tech
}


###########################################################################
# Global variables definition
###########################################################################
SEQPLACE='null' # Sequencing place
SEQTECH='illumina'
RUNID='null'    # Run Id
FLAG_ALL=1      # 1=YES, 0=NO
VERBOSE=1       # 1=YES, 0=NO
BASEPATH='/data2/COVID19/analysis'
SCRIPTS_DIR='/data2/COVID19/software/sars-cov2-mapping/utils'


###########################################################################
# Preconditions
###########################################################################
if [ $# -eq 2 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ $# -eq 3 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
    FLAG_ALL=0   # Only the specified seq place and runID
    VERVOSE=1    # In this mode, the verbose mode is on
elif [ "$1" = "-all" ]; then
    FLAG_ALL=1
    VERBOSE=0    # In this mode the verbose mode is off
else
	echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
	showUsage
fi
###########################################################################


###########################################################################
# Update the metadata files only for the given seqplace & run_id
###########################################################################
if [ $FLAG_ALL -eq 0 ]; then
    updateMetadata
    exit 0
fi

# Check for errors
if [ $FLAG_ALL -ne 1 ]; then
    echo '[ERROR]: Unknown command options'
    showUsage
fi
###########################################################################


###########################################################################
# From here, it is assumed the -all flag 
###########################################################################

# For each sequencing place folder
for seq_place in $(ls $BASEPATH)
do

    # Get the full path to the Sequencing Place
    path01=${BASEPATH}'/'$seq_place

    # For each run folder
    doAnalysis_Folder

done # end seqplace


# Debug
echo -e 'All metadata updating... [DONE]'
echo -e ''
