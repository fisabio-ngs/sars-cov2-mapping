#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Script usefull for preparing the data for a new microreact submission
'''

#---------------------------------------------------------------
__author__      = 'Santiago Jiménez-Serrano'
__credits__     = ['Santiago Jiménez-Serrano']
__copyright__   = 'Copyright 2020, SeqCOVID-Spain Consortium'
__email__       = 'bioinfo.covid19@ibv.csic.es'
#---------------------------------------------------------------


import sys
import os.path
import argparse
import datetime
#from utils.COVID_SeqCOVID_Reports import count_mutations
import COVID_Utils             as cutils
import COVID_SpainMap_Utils    as sp_map_utils
import COVID_SQL_Queries       as dbqueries


def parse_args():
    '''
    Parse arguments given to script
    '''

    parser = argparse.ArgumentParser(description='Prepares the data for a new microreact submission')    
    parser.add_argument('-silent', dest='silent', type=cutils.str2bool, nargs='?', const=True, default=False, help='Specify silent mode')
    parser.add_argument('-csv', dest='csv', required=False, default='null')
    parser.add_argument('-fli', dest='fli', required=False, default='null')
    args = parser.parse_args()

    return args


def doNotInclude(COV_info_record):
    '''
    Gets a flag value if the specified record is an exception 
    that do not have to be included
    '''
    cov = COV_info_record['s.cov_id']

    if  'COV010963' in cov or \
        'COV012374' in cov or \
        'COV013448' in cov or \
        'COV016042' in cov or \
        'COV020488' in cov or \
        'COV020519' in cov or \
        'COV020544' in cov or \
        'COV020764' in cov:
        return True
    return False


def parse_SQL_row(sql_record, silent):
    '''
    Given the data returned by the SQL query, parse it, clean it
    and store into a dict
    '''

    # Initialize the dictionary
    COV_info = {}

    # Check the table status
    COV_info['RESULT'] = cutils.getSQLTableStatus(sql_record)
    if COV_info['RESULT'] != 'DATA':
        return COV_info

    # Set the unknown value
    unknown_value = 'NA'

    # Parse the main fields
    COV_info['s.cov_id']             = sql_record[ 0]
    COV_info['s.status']             = sql_record[ 1]
    COV_info['s.seq_place']          = sql_record[ 2]
    COV_info['s.seq_tech']           = sql_record[ 3]
    COV_info['s.run_code']           = sql_record[ 4]
    COV_info['s.coverage']           = sql_record[ 5]
    COV_info['s.median_depth']       = sql_record[ 6]
    COV_info['s.lineage']            = sql_record[ 7]
    COV_info['h.name']               = sql_record[ 8]
    COV_info['h.latitude']           = sql_record[ 9]
    COV_info['h.longitude']          = sql_record[10]
    COV_info['s.hospital_date']      = sql_record[11]
    COV_info['s.gender']             = cutils.getSafeGender(sql_record[12], unknown_value)
    COV_info['s.age']                = cutils.getSafeAge(   sql_record[13], unknown_value)
    COV_info['s.residence_city']     = sql_record[14]
    COV_info['s.residence_province'] = sql_record[15]
    COV_info['s.residence_ccaa']     = sql_record[16]
    COV_info['s.residence_country']  = sql_record[17]
    COV_info['s.ccaa_name']          = sp_map_utils.get_CCAA(COV_info['s.residence_province'])
    COV_info['s.ccaa_code']          = sp_map_utils.get_CCAA_ISO3166(COV_info['s.residence_province'])
    COV_info['s.sample_latitude']    = sql_record[18]
    COV_info['s.sample_longitude']   = sql_record[19]
    COV_info['h.address']            = sql_record[20]

    # Microreact only for spanish samples
    country = str(COV_info['s.residence_country'])
    if country and not 'spain' in country.lower():
        COV_info['RESULT'] = 'ERR_NOT_SPAIN'
        return COV_info

    # Get the Sequencing technology
    seq_tech = COV_info['s.seq_tech']
    

    # Detailed Sample date values
    dt, yy, mm, dd, epiw = cutils.getDetailedDate(COV_info['s.hospital_date'], unknown_value)
    COV_info['s.hosp_date']          = dt
    COV_info['s.hosp_date_year']     = yy
    COV_info['s.hosp_date_month']    = mm
    COV_info['s.hosp_date_day']      = dd    
    COV_info['s.hosp_date_epi_week'] = epiw  # Get the EPI Week


    # Local variable
    cov = COV_info['s.cov_id']

    # Files placement
    COV_info['s.analysis_folder']        = cutils.getAnalysisFolder(      COV_info['s.seq_place'], COV_info['s.run_code'], seq_tech)
    COV_info['s.sequencing_data_folder'] = cutils.getSequencingDataFolder(COV_info['s.seq_place'], COV_info['s.run_code'], seq_tech)
    COV_info['s.aln_masked_path']        = "{}/consensus/{}.aln.masked.fas".format(COV_info['s.analysis_folder'], cov)

    # Get the sample origin Province & City from the Hospital Address
    hprov, hcity, hccaa_name, hccaa_code = cutils.getInfoFromHospitalAddress(COV_info['h.address'], cov)    
    COV_info['h.ori_PROV']  = hprov
    COV_info['h.ori_CITY']  = hcity
    COV_info['h.ccaa_name'] = hccaa_name
    COV_info['h.ccaa_code'] = hccaa_code    

    # If Residence City or Province are unknown, set the hospital ones plus a '_h' suffix
    COV_info['s.residence_city']     = cutils.getSafeRCity(    COV_info['s.residence_city'],     hcity, cov, silent)
    COV_info['s.residence_province'] = cutils.getSafeRProvince(COV_info['s.residence_province'], hprov, cov, silent)

    # If Sample Coordinates are unknown, set the hospital ones
    COV_info['s.sample_latitude'],  COV_info['s.sample_longitude'] =       \
        cutils.getSafeSampleCoords(                                        \
            COV_info['s.sample_latitude'], COV_info['s.sample_longitude'], \
            COV_info['h.latitude'],        COV_info['h.longitude'],        \
            cov, silent)

    # All went ok
    COV_info['RESULT'] = 'OK'
    return COV_info


def parse_SQL_table(SQL_info, silent):
    '''
    Parse the whole datatable to a list of dictionaries. 1 dict per cov sample
    '''

    # Initialize the list
    COV_info_list = []

    # Check the table status
    if cutils.getSQLTableStatus(SQL_info) != 'DATA':
        return COV_info_list

    print('[INFO] Parsing sql datataset...')

    num_errors    = 0
    num_excluded  = 0
    num_not_spain = 0
    dic_not_spain = {}

    # For each row, append the corresponding dictionary
    for sql_record in SQL_info:

        # Parse the row to a dictionary
        COV_info_record = parse_SQL_row(sql_record, silent)

        if 'ERR_NOT_SPAIN' in COV_info_record['RESULT']:
            num_not_spain += 1
            country = COV_info_record['s.residence_country']
            if not country in dic_not_spain:
                dic_not_spain[country] = 0
            dic_not_spain[country] += 1
            continue

        # Get the corresponding fasta file path
        fas = COV_info_record['s.aln_masked_path']

        # Get the cov identifier
        cov = COV_info_record['s.cov_id']

        # Append to the list only if the fasta file exist
        if os.path.isfile(fas):

            if doNotInclude(COV_info_record):
                num_excluded += 1
                # Print warning
                #if not silent:
                warning_str = "W[{}]: => COV in exclude list. Sample not appened! ({})\n".format(cov, fas)
                sys.stderr.write(warning_str)

            else:
                # Include this sample in the list
                COV_info_list.append(COV_info_record)
        else:
            num_errors += 1
            # Print warning
            #if not silent:            
            warning_str = "W[{}]: => Fasta file does not exist. Sample not appened! ({}) \n".format(cov, fas)
            sys.stderr.write(warning_str)

    # Print some debug
    print('[INFO] #rows successfully parsed:         ', len(COV_info_list))
    print('[INFO] #rows with no fasta in filesystem: ', num_errors)
    print('[INFO] #rows excluded:                    ', num_excluded)
    print('[INFO] #rows excluded (not Spain):        ', num_not_spain)
    for country in dic_not_spain:
        print('\t', country, ' => #samples = ', str(dic_not_spain[country]))
        
    # Return the list
    return COV_info_list


def SQL_query(args):
    ''' 
    Connect to the DB, retrieve info for a give COV
    '''                     

    # Get all the SQL info
    SQL_info = dbqueries.Execute_SELECT_microreact_info()

    # Pass SQL_info to a dictionary
    COV_info = parse_SQL_table(SQL_info, args.silent)

    # Return the dictionary
    return COV_info


def get_csv_header():
    '''
    Returns the header for the csv file
    '''

    return \
        'ID;coverage;median_depth;lineage__autocolour;Hospital;latitude;longitude;year;month;day;epi_week;Gender__autocolour;Age;Residence_city\n' + \
        'hCoV-19_Wuhan_IPBCAMS-WH-01_2019_EPI_ISL_402123_2019-12-24;NA;NA;Wuhan_191204;NA;NA;NA;NA;NA;NA;NA;NA;NA;\n'


def get_csv_line(cov_dict):
    '''
    Returns the csv line corresponding to the specified cov sample
    '''

    # Round the coverage value
    if cov_dict['s.coverage']:
        coverage = str(round(cov_dict['s.coverage'], 3))
    else:
        coverage = 'NA'

    # Round the median depth value
    if cov_dict['s.median_depth']:
        median_depth = str(int(cov_dict['s.median_depth']))
    else:
        median_depth = 'NA'

    # Round the latitude value
    if cov_dict['s.sample_latitude']:
        lat = str(round(cov_dict['s.sample_latitude'], 10))
    else:
        lat = 'NA'

    # Round the longitude value
    if cov_dict['s.sample_longitude']:
        lon = str(round(cov_dict['s.sample_longitude'], 10))
    else:
        lon = 'NA'

    # Return the csv line string
    return \
    '{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n'.format( \
        cov_dict['s.cov_id'],             \
        coverage,                         \
        median_depth,                     \
        cov_dict['s.lineage'],            \
        cov_dict['h.name'],               \
        lat,                              \
        lon,                              \
        cov_dict['s.hosp_date_year'],     \
        cov_dict['s.hosp_date_month'],    \
        cov_dict['s.hosp_date_day'],      \
        cov_dict['s.hosp_date_epi_week'], \
        cov_dict['s.gender'],             \
        cov_dict['s.age'],                \
        cov_dict['s.residence_city']      \
    )


def write_csv(args, COV_info):
    '''
    Write the dataset to the csv needed to perform a submit to microreact
    '''

    # Set the output path
    csvpath = args.csv
    if csvpath == 'null':
        dt = datetime.datetime.now().strftime('%Y%m%d')
        csvpath = './MICROREACT_metadata_{}.csv'.format(dt)

    # Debug
    print('[INFO] Writing csv file to:', csvpath)

    # Write the file [header + data]
    with open(csvpath, "w") as outfile:    
        outfile.write(get_csv_header())
        for cov in COV_info:    
            outfile.write(get_csv_line(cov))

    # Debug
    print('[INFO] Writing csv file: [DONE]')


def write_fasta_filelist(args, COV_info):
    '''
    Write each fasta filename needed to perform a submit to microreact to a file
    '''

    # Set the output path
    flipath = args.fli
    if flipath == 'null':
        dt = datetime.datetime.now().strftime('%Y%m%d')
        flipath = './MICROREACT_fasta_{}.list'.format(dt)

    # Debug
    print('[INFO] Writing fasta files list to:', flipath)

    # Write the file [a line for each existing fasta file]
    with open(flipath, "w") as outfile:            
        for cov in COV_info:    
            outfile.write(cov['s.aln_masked_path'])
            outfile.write('\n')

    # Debug
    print('[INFO] Writing fasta files list: [DONE]')


def main():
    '''
    Main entry point
    '''

    # Get the args
    args = parse_args()

    # Query to the database and parse info
    COV_info = SQL_query(args)
    
    # Write the csv file
    write_csv(args, COV_info)

    # Write the fasta file list file
    write_fasta_filelist(args, COV_info)

    # Debugging lines
    #for record in COV_info:
    #    cutils.print_cov_record_info(record)
    #
    #print(get_csv_header(), end='')
    #for record in COV_info:
    #    print(get_csv_line(record), end='')

    return 0


if __name__ == '__main__':
    main()
