#!/usr/bin/bash
###############################################################################
## Usage: COVID-microreact-covlist.sh <covlist.txt>
##
## Arguments:
##   <covlist.txt> Path to a text file containing a list of COVxxxxxx ids
## 
## Description:
##   Performs a new microreact analysis for the given cov list.
##   As results, generates a new forlder containing:
##     .csv      file: Sample metadata
##     .fasta    file: Concatenated pseudoconsensus sample files
##     .filelist file: Full paths used to create the fasta file
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 11/09/2020
## __version__   = 1.0.0
##
###############################################################################

# Auxiliar functions ##########################################################
function showUsage()
{
    echo -e 'Usage: ' $1 ' <covlist.txt>'
    echo -e '\t Performs a new microreact analysis for the given cov list'
    exit -1
}

function getCOVInfoField()
{
    echo $(cat $1 | grep $2 | cut -f2 -d':')
}
###############################################################################


# Preconditions ###############################################################
# Check for input arguments
if [ $# != 1 ]; then
    showUsage $0
fi

# Test if input file exist
if [ ! -f $1 ]; then
    echo -e "[ERROR]: Input <covlist> file does not exist: " $1
    exit -1
fi
###############################################################################

# Initialization ##############################################################
# Constants
date=$(date +%y%m%d_%H%M)
mypath=$(pwd)
folder=microreact-covlist-results-$date
covlist=$mypath/$folder/covlist.txt
csv_path=$mypath/$folder/MICROREACT_covlist_metadata_$date.csv
pse_flist_path=$mypath/$folder/MICROREACT_covlist_pseudoconsensus_$date.filelist
pse_fasta_path=$mypath/$folder/MICROREACT_covlist_pseudoconsensus_$date.fasta
analysis_path=/data2/COVID19/analysis

# Create the results-folder structure
mkdir $folder
mkdir $folder/cov-info

# Sort the covlist and check for no repetitions
cat $1 | sort | uniq > $covlist
###############################################################################

# 1 - Get all the information of the COVs in the list #########################
for i in $(cat $covlist); do

    # Construct the output file path
    out_file=$mypath/$folder/cov-info/$i.info
    
    # Debug
    echo -e '\t [' $i '] => Getting info from DB'

    # Get the COVxxxxxx info from DB and save it to a file    
    COVID_getCOVSampleInfo.py -cov $i > $out_file

    # Get the RESULT code
    result=$(getCOVInfoField $out_file RESULT)

    # Check the RESULT code
    if [ "$result" != "OK" ]; then
        echo -e "[ERROR]: COV not in database or error connecting to DB => " $i
        exit -1
    fi
done
###############################################################################


# 2 - Create the CSV for microreact ###########################################
# GENERATE the HEADER for the MICROREACT CSV
echo "ID;coverage;median_depth;lineage__autocolour;Hospital;latitude;"\
"longitude;year;month;day;epi_week;Gender__autocolour;Age;Residence_city" > $csv_path
# Add whuan strain line
cat $analysis_path/general_results/MICROREACT/wuhan_line.csv >> $csv_path

for i in $(cat $covlist); do
    
    # Construct the input file path
    in_file=$mypath/$folder/cov-info/$i.info

    # Debug
    echo -e '\t [' $i '] => Saving CSV info'
    
    # Get the Fields for the given COVxxxxxx
    f01=$(getCOVInfoField $in_file s.cov_id)             # ID
    f02=$(getCOVInfoField $in_file s.coverage)           # coverage
    f03=$(getCOVInfoField $in_file s.median_depth)       # median_depth
    f04=$(getCOVInfoField $in_file s.lineage)            # lineage__autocolour
    f05=$(getCOVInfoField $in_file h.name)               # Hospital
    f06=$(getCOVInfoField $in_file s.sample_latitude)    # latitude
    f07=$(getCOVInfoField $in_file s.sample_longitude)   # longitude
    f08=$(getCOVInfoField $in_file s.hosp_date_year)     # year
    f09=$(getCOVInfoField $in_file s.hosp_date_month)    # month
    f10=$(getCOVInfoField $in_file s.hosp_date_day)      # day
    f11=$(getCOVInfoField $in_file s.hosp_date_epi_week) # epi_week
    f12=$(getCOVInfoField $in_file s.gender)             # Gender__autocolour
    f13=$(getCOVInfoField $in_file s.age)                # Age
    f14=$(getCOVInfoField $in_file s.residence_city)     # Residence_city
    
    # Save the line to the csv file
    echo $f01';'$f02';'$f03';'$f04';'$f05';'$f06';'$f07';'$f08';'$f09';'$f10';'$f11';'$f12';'$f13';'$f14 >> $csv_path
done
###############################################################################




# 2 - Create the PseudoConsensus fasta ########################################

# Append the wuhan pseudoconsensus
cat $analysis_path/general_results/Pseudoconsensus/wuhan_strain.fasta > $pse_fasta_path

# Write the header in the filelist
echo "COV;Pseudoconsensus_path" > $pse_flist_path

for i in $(cat $covlist); do

    # Construct the input file path
    in_file=$mypath/$folder/cov-info/$i.info

    # Debug
    echo -e '\t [' $i '] => Appending fasta pseudoconsensus'    

    # Get the Fields for the given COVxxxxxx
    cov=$(getCOVInfoField $in_file s.cov_id)          # ID
    afl=$(getCOVInfoField $in_file s.analysis_folder) # Analysis folder

    # Get the expected full pseudoconsensus fasta path    
    fasta_path=${afl}/pseudoconsensus/${cov}.pseudoconsensus.fasta

    # Check that the file exists
    if [ ! -f $fasta_path ]; then
        echo -e "[ERROR]: COV pseudoconsensus not found => " $fasta_path
        continue
        #echo -e "[FATAL ERROR] ==> Program aborted"
        #exit -1
    fi

    # Concatenate all pseudoconsensus in the cov list (path)
    echo ${cov}';'$fasta_path >> $pse_flist_path

    # Concatenate this files in a single multifasta
    cat ${fasta_path}  >> $pse_fasta_path

done

# Success log
echo -e '[SUCCESS]'
echo -e '\t Microreact CSV file:       ' $csv_path
echo -e '\t PseudoConsensus file list: ' $pse_flist_path
echo -e '\t PseudoConsensus fasta:     ' $pse_fasta_path
echo -e 

###############################################################################


## Generate the tree with iqtree ##############################################
#
#pse_tree_path=$mypath/$folder/MICROREACT_covlist_tree_$date
#
## Create the tree (uncomment as needed)
#/data/Software/iqtree-1.6.10-Linux/bin/iqtree \
#    -s $pse_fasta_path \
#    -m HKY -pre $pse_tree_path -nt 12
#
## Rename output to nwk extension
#mv $pse_tree_path.treefile $pse_tree_path.nwk
###############################################################################