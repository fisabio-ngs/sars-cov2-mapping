#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Gets the MICROREACT sample locations
Replace Hospital Coordinates by the Residence City ones
WARNING: DEPRECATED
@deprecated Check if this script is needed with the new DB Versoin
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import os


pythonscript_path = os.path.dirname(os.path.realpath(__file__))
micro_file = sys.argv[1]
coord_file = "{}/RECITY_COORDS_completed.txt".format(pythonscript_path)

recity_coords = {}
with open(coord_file) as infile:
    for line in infile:
        recity, coords = line.strip().split(":")
        lat, lon = coords.split(",")
        recity_coords[recity] = (lat, lon)

with open(micro_file) as infile:
    sys.stdout.write(infile.readline())
    recity_seen = set()
    for line in infile:
        line =  line.rstrip().split(";")
        cov, coverage, depth, lin, hosp, lat, lon, y, m, d, week, gender, age, recity = line
        recity = recity.rstrip("_h").lower().replace(" ", "_").replace("-", "_")
        if recity in recity_coords:
            newlat, newlon = recity_coords[recity]
            if newlat != "NA" and newlon != "NA":
                lat = newlat
                lon = newlon
        else:
            if recity not in recity_seen:
                sys.stderr.write("Unknown coordinates for {}\n".format(recity))
                recity_seen.add(recity)
        outline = "{};{};{};{};{};{};{};{};{};{};{};{};{};{}\n".format(
        cov,
        coverage,
        depth,
        lin,
        hosp,
        lat,
        lon,
        y,
        m,
        d,
        week,
        gender,
        age,
        recity
        )
        sys.stdout.write(outline)
