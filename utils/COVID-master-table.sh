#! /bin/sh
ls *R1*.fastq.gz | while read FQ1
 do
   FQ2=$(echo "${FQ1}" | sed 's/R1/R2/')
   sample=$(echo "${FQ1}" | cut -f1 -d'_')
   echo "${sample};${FQ1};${FQ2}"
 done
echo ''