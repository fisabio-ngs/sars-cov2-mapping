###############################################################################
## Usage: COVID_CheckRun.sh <SEQPLACE> <RUNID> [SEQTECH]
##
## Arguments:
##   <SEQPLACE>        Sequencing place for the RUNID. E.g.: FISABIO, IBV, MAD_GM, ...
##   <RUNID>           Run identifier. E.g: MAD_GM_E0105, 200908_M02918, ...
##   [SEQTECH]         Sequencing technology (illumina by default)
## 
## Description:
##   Check the metadata for the given run
##   As results, generates the next set of files:
##     [...]/sequencing_data/<seqplace>/[SEQTECH]/<runid>/Check01_previous_status.txt
##     [...]/sequencing_data/<seqplace>/[SEQTECH]/<runid>/Check02_not_index_sample.txt
##     [...]/sequencing_data/<seqplace>/[SEQTECH]/<runid>/Check03_samples_locations.txt
##
##
## __author__    = "Santiago Jiménez-Serrano" 
## __copyright__ = "Copyright 2020, SeqCOVID-Spain Consortium"
## __email__     = "bioinfo.covid19@ibv.csic.es"
## __date__      = 30/09/2021
## __version__   = 1.0.0
##
###############################################################################


###############################################################################
# Show usage and exit
###############################################################################
function showUsage()
{    
    echo -e 'Usage: COVID_CheckRun.sh <SEQPLACE> <RUNID> [SEQTECH]'
    echo -e ' Description:'
    echo -e '   Checks the files and info before running the analysis for the given run'
    echo -e '   As results, generates a set Check_xxx.txt files:'
    echo -e ''
    exit
}

###############################################################################
# Print success and exit
###############################################################################
function print_success()
{
    echo -e 'COVID_InitRun: [DONE]'
    exit
}

###############################################################################
# Auxiliar functions
###############################################################################
function getCOVInfoField()
{
    echo $(cat $1 | grep -F $2 | cut -f2 -d':')
    # sample -> f1=$(getCOVInfoField ${COVINFO} s.hospital_date)
}


###############################################################################
# Main instructions
###############################################################################

# Global variables
BASEPATH='/data2/COVID19'
SEQPLACE=null       # Sequencing place
RUNID=null          # Run Id
SEQTECH='illumina'  # Sequencing technology (default: illumina)


# Check parameters
if [ $# -eq 2 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
elif [ $# -eq 3 ]; then
    SEQPLACE=$1  # Argument 1
    RUNID=$2     # Argument 2
    SEQTECH=$3   # Argument 3
else
    echo -e '\n[ERROR] Wrong number of arguments or incorrect use of them \n'
    showUsage
fi

# Check for special case
seqplacedir=$SEQPLACE
if [ "$SEQPLACE" = "FISABIO" ]; then
    seqplacedir='seqfisabio'
fi

# Temporal variables
ana='analysis'
seq='sequencing_data'

# Get the folder paths
ana_dir=${BASEPATH}'/'${ana}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}
seq_dir=${BASEPATH}'/'${seq}'/'${seqplacedir}'/'${SEQTECH}'/'${RUNID}

# Get the master table name
master_table=${seq_dir}'/master_table_'${RUNID}'.txt'
delim=';'
if [ "$SEQTECH" = "nanopore" ]; then
    master_table=${seq_dir}'/SampleSheet_'${RUNID}'.csv'
    delim=','
fi


# Get the output files path
fout01=${seq_dir}'/Check01_previous_status.txt'
fout02=${seq_dir}'/Check02_not_index_sample.txt'
fout03=${seq_dir}'/Check03_samples_locations.txt'
fout04=${seq_dir}'/Check04_R1_R2_files_not_exist.txt'


# 1 - Check status #####################################################################
echo '01 - Checking status...'
for i in $(cat ${master_table} | grep COV)
do 
    cov=$(echo $i | cut -f1 -d"${delim}")

    echo -n ${cov} '- '
    COVID_getCOVSampleInfo.py -cov ${cov} -silent 1 | grep status | cut -f2 -d':'  
done > ${fout01}
echo 'DONE -> ' ${fout01}
cat ${fout01}
echo



# 2 - Check not index
echo '02 - Checking not index samples ...'
for i in $(cat ${master_table} | grep COV)
do
    cov=$(echo $i | cut -f1 -d"${delim}")
    echo -n ${cov} ' - IndexSample = '
    COVID_getCOVSampleInfo.py -cov ${cov} -silent 1 | grep index | cut -f2 -d':'
done | grep -v None > ${fout02}
echo 'DONE -> ' ${fout02}
cat ${fout02}
echo



COVINFO='./cov.info'

echo '03 - Checking locations data ...'
echo -e 'COV\tresidence_city\tresidence_province\tresidence_ccaa\tresidence_country\tlatitude\tlongitude' > ${fout03}
for i in $(cat ${master_table} | grep COV)
do

    cov=$(echo $i | cut -f1 -d"${delim}")

    # Get the cov info from the DB
    COVID_getCOVSampleInfo.py -cov $cov -silent 1 -cov ${cov} > ${COVINFO}

    # Get the cov DB info related
    f1=$(getCOVInfoField ${COVINFO} s.residence_city)
    f2=$(getCOVInfoField ${COVINFO} s.residence_province)
    f3=$(getCOVInfoField ${COVINFO} s.ccaa_name)
    f4=$(getCOVInfoField ${COVINFO} s.residence_country)
    f5=$(getCOVInfoField ${COVINFO} s.sample_latitude)
    f6=$(getCOVInfoField ${COVINFO} s.sample_longitude)

    echo -e ${cov}'\t'${f1}'\t'${f2}'\t'${f3}'\t'${f4}'\t'${f5}'\t'${f6} 

    rm ${COVINFO}

done > ${fout03}
echo 'DONE -> ' ${fout03}
cat ${fout03}
echo



if [ "$SEQTECH" = "nanopore" ]; then
    # Next steps will work only for Illumina runs
    exit
fi


echo '04 - Checking file exists ...'
for i in $(cat ${master_table})
do

    cov=$(echo $i | cut -f1 -d"${delim}")
    fq1=$(echo $i | cut -f2 -d"${delim}")
    fq2=$(echo $i | cut -f3 -d"${delim}")

    if [ ! -f $fq1 ]; then
        echo '[ERROR]: Fastq R1 in master table does not exist in file system: ' ${cov} '-> '${fq1} >> ${fout04}
    fi;

    if [ ! -f $fq2 ]; then
        echo '[ERROR]: Fastq R2 in master table does not exist in file system: ' ${cov} '-> '${fq2} >> ${fout04}
    fi;
    
done 

if [ -f ${fout04} ]; then
    cat ${fout04}
    echo
fi
