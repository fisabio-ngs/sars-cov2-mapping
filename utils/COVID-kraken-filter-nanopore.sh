#!/bin/bash


# Input parameters
folder_fastq_pass=$1
folder_fastq_filt=$2
BARCODE=$3
fastqfile=$4
KRAKEN_DB=$5
THREADS=$6

# Debug
echo 'COVID-kraken-filter ==> ' $BARCODE ' - ' $fastqfile

# Removal of reads classified as Homo Sapiens (taxid: 9606) by Kraken.

# Input file paths (fastq.gz)
INFQ=${folder_fastq_pass}'/'${BARCODE}'/'${fastqfile}

# Get the base output filename
basename=$(echo "${fastqfile}" | sed 's/.fastq.gz//g')
basename=${folder_fastq_filt}'/'${BARCODE}'/'${basename}

# Output file paths
outfastq=${basename}'.humanfilt.fastq'
outkraken=${basename}'.kraken'	
outkraken_hids=${outkraken}'.human.ids'

# Debug
#echo
#echo "-----------------------------------------------------------------"
#echo "	Removal of human reads with kraken: ${BARCODE}/${fastqfile}    "
#echo "-----------------------------------------------------------------"

# First taxonomically classify the reads
/data/Software/Kraken/kraken   \
    --db ${KRAKEN_DB}       \
    --fastq-input           \
    --output ${outkraken}   \
    --threads ${THREADS}    \
    --gzip-compressed ${INFQ} 2> /dev/null


# Get the identifiers of reads that belong to human (taxid: 9606)
awk '$3 == "9606" { print $2 }' ${outkraken} > ${outkraken_hids}


# Pick reads from a fastq given a list of readids using ad-hoc script
Script-fasta-pick-readlist.py \
    -in ${INFQ}               \
    -r ${outkraken_hids}      \
    -a discard                \
    -o ${outfastq}            \
    -f fastq -c 2> /dev/null

# Gzip fastq file
gzip -f ${outfastq}

#remove intermediate files
rm ${outkraken_hids}

echo '[DONE] COVID-kraken-filter  ==> ' $BARCODE ' - ' $fastqfile