#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utility Functions to manage Spain-Map info
'''
#---------------------------------------------------------------
__author__      = 'Galo A. Goig, Santiago Jiménez-Serrano'
__credits__     = ['Galo A. Goig', 'Santiago Jiménez-Serrano']
__copyright__   = 'Copyright 2020, SeqCOVID-Spain Consortium'
__email__       = 'bioinfo.covid19@ibv.csic.es'
#---------------------------------------------------------------



#---------------------------------------------------------------
# Static variables
#---------------------------------------------------------------

CCAA_dict={
        'alava'          : 'Basque_Country',
        'araba'          : 'Basque_Country',
        'albacete'       : 'Castilla_La_Mancha',
        'alicante'       : 'Comunitat_Valenciana',
        'alacant'        : 'Comunitat_Valenciana',
        'almeria'        : 'Andalusia',
        'asturias'       : 'Asturias',
        'avila'          : 'Castilla_y_Leon',
        'badajoz'        : 'Extremadura',        
        'barcelona'      : 'Catalunya',
        'burgos'         : 'Castilla_y_Leon',
        'caceres'        : 'Extremadura',
        'cadiz'          : 'Andalusia',
        'cantabria'      : 'Cantabria',
        'castellon'      : 'Comunitat_Valenciana',
        'castello'       : 'Comunitat_Valenciana',
        'ciudad_real'    : 'Castilla_La_Mancha',
        'cordoba'        : 'Andalusia',
        'coruna'         : 'Galicia',
        'la_coruna'      : 'Galicia',
        'a_coruna'       : 'Galicia',
        'cuenca'         : 'Castilla_La_Mancha',
        'girona'         : 'Catalunya',
        'granada'        : 'Andalusia',
        'guadalajara'    : 'Castilla_La_Mancha',
        'guipuzcoa'      : 'Basque_Country',
        'gipuzkoa'       : 'Basque_Country',        
        'huelva'         : 'Andalusia',
        'huesca'         : 'Aragon',
        'islas_baleares' : 'Balear_Islands',
        'illes_balears'  : 'Balear_Islands',
        'baleares'       : 'Balear_Islands',
        'balears'        : 'Balear_Islands',
        'jaen'           : 'Andalusia',
        'leon'           : 'Castilla_y_Leon',
        'lleida'         : 'Catalunya',
        'lugo'           : 'Galicia',
        'madrid'         : 'Madrid',
        'malaga'         : 'Andalusia',
        'murcia'         : 'Murcia',
        'navarra'        : 'Navarra',
        'ourense'        : 'Galicia',
        'orense'         : 'Galicia',
        'palencia'       : 'Castilla_y_Leon',
        'las_palmas'     : 'Canary_Islands',
        'pontevedra'     : 'Galicia',
        'la_rioja'       : 'La_Rioja',
        'salamanca'      : 'Castilla_y_Leon',
        'segovia'        : 'Castilla_y_Leon',
        'sevilla'        : 'Andalusia',
        'soria'          : 'Castilla_y_Leon',
        'tarragona'      : 'Catalunya',
        'santa_cruz_de_tenerife':'Canary_Islands',
        'teruel'         : 'Aragon',
        'toledo'         : 'Castilla_La_Mancha',
        'valencia'       : 'Comunitat_Valenciana',
        'valladolid'     : 'Castilla_y_Leon',
        'vizcaya'        : 'Basque_Country',
        'bizkaia'        : 'Basque_Country',
        'zamora'         : 'Castilla_y_Leon',
        'zaragoza'       : 'Aragon',
        'ceuta'          : 'Ceuta',
        'melilla'        : 'Melilla'
    }

'''
Dictionary with the ISO 3166-2:ES Province => CCAA code correspondence
https://en.wikipedia.org/wiki/ISO_3166-2:ES
'''
CCAA_ISO3166_dict={
        'alava'          : 'PV',
        'araba'          : 'PV',
        'albacete'       : 'CM',
        'alicante'       : 'VC',
        'alacant'        : 'VC',
        'almeria'        : 'AN',
        'asturias'       : 'AS',
        'avila'          : 'CL',
        'badajoz'        : 'EX',
        'barcelona'      : 'CT',
        'burgos'         : 'CL',
        'caceres'        : 'EX',
        'cadiz'          : 'AN',
        'cantabria'      : 'CB',
        'castellon'      : 'VC',
        'castello'       : 'VC',
        'ciudad_real'    : 'CM',
        'cordoba'        : 'AN',
        'coruna'         : 'GA',
        'la_coruna'      : 'GA',
        'a_coruna'       : 'GA',
        'cuenca'         : 'CM',
        'girona'         : 'CT',
        'granada'        : 'AN',
        'guadalajara'    : 'CM',
        'guipuzcoa'      : 'PV',
        'gipuzcoa'       : 'PV',
        'gipuzkoa'       : 'PV',
        'huelva'         : 'AN',
        'huesca'         : 'AR',
        'islas_baleares' : 'IB',
        'illes_balears'  : 'IB',        
        'baleares'       : 'IB',
        'balears'        : 'IB',
        'jaen'           : 'AN',
        'leon'           : 'CL',
        'lleida'         : 'CT',
        'lugo'           : 'GA',
        'madrid'         : 'MD',
        'malaga'         : 'AN',
        'murcia'         : 'MC',
        'navarra'        : 'NC',
        'ourense'        : 'GA',
        'orense'         : 'GA',
        'palencia'       : 'CL',
        'las_palmas'     : 'CN',
        'pontevedra'     : 'GA',
        'la_rioja'       : 'RI',
        'salamanca'      : 'CL',
        'segovia'        : 'CL',
        'sevilla'        : 'AN',
        'soria'          : 'CL',
        'tarragona'      : 'CT',
        'santa_cruz_de_tenerife' : 'CN',
        'teruel'         : 'AR',
        'toledo'         : 'CM',
        'valencia'       : 'VC',
        'valladolid'     : 'CL',
        'vizcaya'        : 'PV',
        'bizkaia'        : 'PV',
        'zamora'         : 'CL',
        'zaragoza'       : 'AR',
        'ceuta'          : 'CE',
        'melilla'        : 'ML'
    }


'''
Dictionary that links the Hospital Code's prefix (First 4 letters) with the corresponding CCAA
'''
CCAA_hosp_code_dict ={
        'AND_' : 'Andalucía',
        'ARA_' : 'Aragón',
        'AST_' : 'Asturias',
        'BAL_' : 'Illes Balears',        
        'CANA' : 'Islas Canarias',
        'CANT' : 'Cantabria',
        'CAT_' : 'Catalunya', 
        'CLM_' : 'Castilla-La Mancha',
        'CVA_' : 'Comunitat Valenciana',         
        'CYL_' : 'Castilla y León',
        'EXT_' : 'Extremadura',
        'GAL_' : 'Galicia',
        'MAD_' : 'Madrid',
        'MUR_' : 'Murcia',
        'NAV_' : 'Navarra',
        'PVA_' : 'País Vasco',
        'RIO_' : 'La Rioja',        
        'CEU_' : 'Ceuta',
        'MEL_' : 'Melilla',
        'PT_I' : 'Portugal'
    }


'''
Spanish 'Comunidades Autonomas' in the db (sample.residence_ccaa)
'''
CCAA_spain_db = [
    'Andalucia', 
    'Aragon', 
    'Asturias', 
    'Canarias', 
    'Cantabria', 
    'Castilla y Leon', 
    'Castilla-La Mancha', 
    'Catalunya', 
    'Ceuta', 
    'Comunitat Valenciana', 
    'Extremadura', 
    'Galicia', 
    'Islas Baleares', 
    'La Rioja', 
    'Madrid', 
    'Melilla', 
    'Murcia', 
    'Navarra', 
    'Pais Vasco' ]



#---------------------------------------------------------------
# Functions
#---------------------------------------------------------------


def get_CCAA(province):
    '''
    Get the CCAA according to the specified province
    '''    
    if not province:
        return ''

    key = province.strip().replace(' ', '_').lower()
    if not exist_spain_province(key):
        return ''
    return CCAA_dict[key]


def get_CCAA_ISO3166(province):
    '''
    Get the CCAA ISO3166 code according to the specified province
    '''
    if not province:
        return ''

    key = province.strip().replace(' ', '_').lower()
    if not exist_spain_province(key):
        return ''
    return CCAA_ISO3166_dict[key]


def get_CCAA_ByHospCodePrefix(hosp_code_prefix):
    '''
    Get the CCAA full name according to the specified hospital code prefix
    '''
    if not hosp_code_prefix:
        return ''

    if not exist_hosp_code_prefix(hosp_code_prefix):
        return ''
    return CCAA_hosp_code_dict[hosp_code_prefix]


def exist_spain_province(province):
    '''
    Gets a value indicating if exists the specified province in the CCAA dictionary
    '''
    key = province.strip().replace(' ', '_').lower()
    return key in CCAA_dict


def exist_hosp_code_prefix(hosp_code_prefix):
    '''
    Gets a value indicating if exists the specified hospital code in the CCAA dictionary
    '''
    return hosp_code_prefix in CCAA_hosp_code_dict