#! /usr/bin/bash

mypath=$(pwd)


basepath='.'
mastertable=$mypath'/master_table_MERGE_FISABIO_0006.txt'

#basepath='/data2/COVID19/sequencing_data/seqfisabio/illumina/MERGE_FISABIO_0002'
#mastertable=$mypath'/master_table_MERGE_FISABIO_0002.txt'


for line in $(cat merged.files.dict.txt)
do
    
    cov=$(echo $line | cut -f 1 -d';' ) # Cov identifier
    f1=$(echo $line  | cut -f 2 -d';' ) # 
    f2=$(echo $line  | cut -f 3 -d';' )
    rr=$(echo $line  | cut -f 4 -d';' ) # R1 | R2
    
    f1=$basepath'/'$f1
    f2=$basepath'/'$f2
    
    # Debug
    echo 'Merging ' $cov '(' $rr ')'
    echo -e ' \t File 1: ' $f1
    echo -e ' \t File 2: ' $f2
    echo

    # Check file 1 exist
    if [ ! -f $f1 ]; then
        echo 'ERROR!!!!! ==> File 1 does not exist. NOT merged with anything.'
        continue
    fi
    
    # Check file 2 exist
    if [ ! -f $f2 ]; then
        echo 'ERROR!!!!! ==> File 2 does not exist. NOT merged with anything.'
        continue
    fi
    

    # Merge !! ##################################################
    echo $f1 $f2 |
    while read FFQ
    do
       zcat ${FFQ}
    done | gzip >> $mypath/$cov.m.$rr.fastq.gz
    #############################################################
    
done

cd $mypath
COVID-master-table.sh > $mastertable





###############################################################################
# Cómo se hizo en el primer run_merged de FISABIO
#
# ls COV*.gz | cut -f1 -d'_' | cut -f1 -d'.'| sort -u > samples.txt
#
# mypath=$(pwd)
# mastertable=master_table_MERGE_FISABIO_0001.txt
# echo 'Estoy en ' $mypath
#
# echo > $mastertable
#
#
# for cov in $(cat samples.txt); do
#     echo 'Merging ' $cov
#
#     f1=$(ls $mypath/$cov* | grep Rep1 | grep _R1)
#     f2=$(ls $mypath/$cov* | grep Rep2 | grep _R1)
#
#     echo ' (1) Rep1: ' $f1 ' -> Rep2: '  $f2
#
#     # Do something
#     # echo $f1 $f2 |
#     # while read FFQ
#     # do
#     #    zcat ${FFQ}
#     # done | gzip > $mypath/$cov.m.R1.fastq.gz    
#
#
#     f1=$(ls $mypath/$cov* | grep Rep1 | grep _R2)
#     f2=$(ls $mypath/$cov* | grep Rep2 | grep _R2)
#     echo ' (2) Rep1: ' $f1 ' -> Rep2: '  $f2
#
#     # Do something else
#     # echo $f1 $f2 |
#     # while read FFQ
#     # do
#     #    zcat ${FFQ}
#     # done | gzip > $mypath/$cov.m.R2.fastq.gz
#
#     echo $cov';'$cov'.m.R1.fastq.gz;'$cov'.m.R2.fastq.gz' >> $mastertable
#   
# done
#
################################################################################################

# cat Resequenced_samples.txt | while read COV
#  do 
#   # Search all previously sequenced samples discarding previously
#   # merged like COVXXXXXm <- Also at this stage we to not take into
#   # account samples already filtered with Kraken, just for convenience
#   # The merged sampled will be classified as a whole again and filtered
#   # accordingly
#   find /data2/COVID19/sequencing_data/ -name "${COV}*.fastq.gz" | 
#   grep -v "COV[0-9]\{6\}m" | grep -v "humanfilt" | grep R1 |
#   while read FFQ
#     do
#       zcat ${FFQ}
#     done | gzip > ${COV}m.R1.fastq.gz
#   # Now the same for reverse reads files 
#   find /data2/COVID19/sequencing_data/ -name "${COV}*.fastq.gz" | 
#   grep -v "COV[0-9]\{6\}m" | grep -v "humanfilt" | grep R2 |
#   while read RFQ
#     do
#       zcat ${RFQ}
#     done | gzip > ${COV}m.R2.fastq.gz
#     echo "${COV};${COV}m.R1.fastq.gz;${COV}m.R2.fastq.gz"
# done > master_table_merged.txt