#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utilities to Get coordinates from residence city
'''
#---------------------------------------------------------------
__author__      = "Santiago Jiménez-Serrano"
__credits__     = ["Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import csv
import argparse


def parse_args():
    '''
    Parse arguments given to script
    '''

    # Input args
    parser = argparse.ArgumentParser(description="Get coordinates from residence city")
    parser.add_argument("-r", "--residence-city", dest="recity_file", required=False, default="./BD_residence_city_list.txt")
    parser.add_argument("-m", "--municipios",     dest="muni_file",   required=False, default="./MUNICIPIOS_notilde.csv")
    parser.add_argument("-e", "--entidades",      dest="enti_file",   required=False, default="./ENTIDADES_notilde.csv")
    args = parser.parse_args()

    # Debug parameters
    print('Cities file (input file to get coordinates): ', args.recity_file)
    print('Municipios file (coordinates):               ', args.muni_file)
    print('Entidades  file (coordinates):               ', args.enti_file)

    return args


def remove_blanks(value):
    value = value.replace(" ", "_").replace("-", "_").lower()    
    if value == 'castellon':
        value = 'castello/castellon'
    elif value == 'vizcaya':
        value = 'bizkaia'
    elif value == 'islas_baleares':
        value = 'illes_balears'
    elif value == 'estella':
        value = 'estella_lizarra'
    elif value == 'alicante':
        value = 'alacant/alicante'
    return value


def parse_municipios_tsv(municipios_tsv_path = './MUNICIPIOS_notilde.tsv'):
    '''
    <>
    '''
    
    db01 = {}    
    db02 = {}
    
    with open(municipios_tsv_path) as infile:
        rd = csv.reader(infile, delimiter='\t')
        for row in rd:
            
            province = remove_blanks(row[0])
            city01   = remove_blanks(row[1])
            city02   = remove_blanks(row[2])

            #print(province, ' - ', city01, ' - ', city02)
            lat      = row[3]
            lon      = row[4]

            # Initialize dictionary
            if not province in db01:
                db01[province] = {}
                db02[province] = {}

            db01[province][city01] = [lat, lon]
            db02[province][city02] = [lat, lon]
            
            #print(province, ' - ', city01, ' - ', city02, ' - ', lat, ' - ', lon)
            

    return db01, db02


def parse_entidades_tsv(entidades_tsv_path = './ENTIDADES_notilde.tsv'):
    '''
    <>
    '''
    
    db01 = {}
    
    with open(entidades_tsv_path) as infile:
        rd = csv.reader(infile, delimiter='\t')
        for row in rd:
            
            province = remove_blanks(row[0])
            city01   = remove_blanks(row[1])            

            #print(province, ' - ', city01)
            lat      = row[2]
            lon      = row[3]

            # Initialize dictionary
            if not province in db01:
                db01[province] = {}

            db01[province][city01] = [lat, lon]
            
            #print(province, ' - ', city01, ' - ', lat, ' - ', lon)
            

    return db01


def parse_data(input_path, db01, db02, db03):
    '''
    <>
    '''

    with open(input_path) as infile:
        rd = csv.reader(infile, delimiter=',', quotechar='"')
        for row in rd:

            #print(row)
            COV                = row[0]
            residence_city     = remove_blanks(row[1])
            residence_province = remove_blanks(row[2])
            #residence_country  = remove_blanks(row[3])



            if residence_province in db01:
                if residence_city in db01[residence_province]:
                    lat, lon = db01[residence_province][residence_city]
                    print(COV, '\t', lat, '\t', lon, '\t', residence_province, '\t', residence_city)
                elif residence_city in db02[residence_province]:
                    lat, lon = db02[residence_province][residence_city]
                    print(COV, '\t', lat, '\t', lon, '\t', residence_province, '\t', residence_city)
                elif residence_city in db03[residence_province]:
                    lat, lon = db03[residence_province][residence_city]
                    print(COV, '\t', lat, '\t', lon, '\t', residence_province, '\t', residence_city)
                #elif residence_city:
                #    print(COV, '\t', 'NO CITY', '\t', residence_province, '\t', residence_city)
            else:
                    print(COV, '\t', 'NO PROV', '\t', residence_province, '\t', residence_city)






'''
OLD!!!!
'''

def parse_recity(recity_file):
    with open(recity_file) as infile:
        for line in infile:
            line = line.strip().lower()
            line = line.replace(" ", "_")
            line = line.replace("-", "_")
            yield line



def parse_municipios(muni_file):

    municipios = {}
    with open(muni_file) as infile:
        infile.readline()
        for line in infile:
            line = line.split(";")
            provincia = line[4].lower().replace(" ", "_")
            municipio = line[5].lower().replace(" ", "_")
            variantes = municipio.split("/")
            lon = line[13]
            lat = line[14]
            
            for variante in variantes:
                municipios[variante] = (lat, lon, provincia)
                
                if "ch" in variante:
                    municipios[variante.replace("ch", "tx")] = lat, lon, provincia
                if "tx" in variante:
                    municipios[variante.replace("tx", "ch")] = lat, lon, provincia

            if "-" in municipio:
                municipios[municipio.replace("-", "_")] = (lat, lon, provincia)
                variantes = municipio.split("-")
                for variante in variantes:
                    municipios[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        municipios[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        municipios[variante.replace("tx", "ch")] = lat, lon, provincia

    return municipios



def parse_entidades(enti_file):

    entidades = {}
    with open(enti_file) as infile:
        infile.readline()
        for line in infile:

            # Read line
            line = line.strip().split(";")
            
            # get the fields
            poblacion = line[1].lower().replace(" ", "_").replace("-", "_")
            provincia = line[3].lower().replace(" ", "_")
            tipo      = line[4]
            lon       = line[8]
            lat       = line[9]
            variantes = poblacion.split("/")

            if tipo == "Entidad_singular" or tipo == "Municipio":
                for variante in variantes:
                    entidades[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        entidades[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        entidades[variante.replace("tx", "ch")] = lat, lon, provincia
            if "-" in poblacion:
                entidades[poblacion.replace("-", "_")] = (lat, lon, provincia)
                variantes = poblacion.split("-") # Vitoria - Gasteiz
                for variante in variantes:
                    entidades[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        entidades[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        entidades[variante.replace("tx", "ch")] = lat, lon, provincia
    return entidades



def get_coordinates(args):    

    municipios = parse_municipios(args.muni_file)
    entidades  = parse_entidades(args.enti_file)

    for recity in parse_recity(args.recity_file):
        if recity in municipios:
            lat, lon, provincia = municipios[recity]
            sys.stdout.write("{};{}:{},{}\n".format(provincia, recity, lat, lon))
        elif recity in entidades:
            lat, lon, provincia = entidades[recity]
            sys.stdout.write("{};{}:{},{}\n".format(provincia, recity, lat, lon))
        else:
            sys.stderr.write("?;{}:NA,NA\n".format(recity))

    return 0



def main():

    db01, db02 = parse_municipios_tsv()
    db03       = parse_entidades_tsv()
    parse_data('/mnt/c/Users/sjimenez/Documents/coordenadas_xxx2.csv', db01, db02, db03)

    #args = parse_args()
    #get_coordinates(args)

    return 0



if __name__ == '__main__':
    main()
    