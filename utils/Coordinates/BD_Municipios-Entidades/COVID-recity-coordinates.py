#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Utilities to Get coordinates from residence city
'''
#---------------------------------------------------------------
__author__      = "Galo A. Goig, Santiago Jiménez-Serrano"
__credits__     = ["Galo A. Goig", "Santiago Jiménez-Serrano"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import sys
import argparse


def parse_args():
    '''
    Parse arguments given to script
    '''

    # Input args
    parser = argparse.ArgumentParser(description="Get coordinates from residence city")
    parser.add_argument("-r", "--residence-city", dest="recity_file", required=False, default="./BD_residence_city_list.txt")
    parser.add_argument("-m", "--municipios",     dest="muni_file",   required=False, default="./MUNICIPIOS_notilde.csv")
    parser.add_argument("-e", "--entidades",      dest="enti_file",   required=False, default="./ENTIDADES_notilde.csv")
    args = parser.parse_args()

    # Debug parameters
    print('Cities file (input file to get coordinates): ', args.recity_file)
    print('Municipios file (coordinates):               ', args.muni_file)
    print('Entidades  file (coordinates):               ', args.enti_file)

    return args



def parse_recity(recity_file):
    with open(recity_file) as infile:
        for line in infile:
            line = line.strip().lower()
            line = line.replace(" ", "_")
            line = line.replace("-", "_")
            yield line



def parse_municipios(muni_file):

    municipios = {}
    with open(muni_file) as infile:
        infile.readline()
        for line in infile:
            line = line.split(";")
            provincia = line[4].lower().replace(" ", "_")
            municipio = line[5].lower().replace(" ", "_")
            variantes = municipio.split("/")
            lon = line[13]
            lat = line[14]
            
            for variante in variantes:
                municipios[variante] = (lat, lon, provincia)
                
                if "ch" in variante:
                    municipios[variante.replace("ch", "tx")] = lat, lon, provincia
                if "tx" in variante:
                    municipios[variante.replace("tx", "ch")] = lat, lon, provincia

            if "-" in municipio:
                municipios[municipio.replace("-", "_")] = (lat, lon, provincia)
                variantes = municipio.split("-")
                for variante in variantes:
                    municipios[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        municipios[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        municipios[variante.replace("tx", "ch")] = lat, lon, provincia

    return municipios



def parse_entidades(enti_file):

    entidades = {}
    with open(enti_file) as infile:
        infile.readline()
        for line in infile:

            # Read line
            line = line.strip().split(";")
            
            # get the fields
            poblacion = line[1].lower().replace(" ", "_").replace("-", "_")
            provincia = line[3].lower().replace(" ", "_")
            tipo      = line[4]
            lon       = line[8]
            lat       = line[9]
            variantes = poblacion.split("/")

            if tipo == "Entidad_singular" or tipo == "Municipio":
                for variante in variantes:
                    entidades[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        entidades[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        entidades[variante.replace("tx", "ch")] = lat, lon, provincia
            if "-" in poblacion:
                entidades[poblacion.replace("-", "_")] = (lat, lon, provincia)
                variantes = poblacion.split("-") # Vitoria - Gasteiz
                for variante in variantes:
                    entidades[variante] = (lat, lon, provincia)
                    if "ch" in variante:
                        entidades[variante.replace("ch", "tx")] = lat, lon, provincia
                    if "tx" in variante:
                        entidades[variante.replace("tx", "ch")] = lat, lon, provincia
    return entidades



def get_coordinates(args):    

    municipios = parse_municipios(args.muni_file)
    entidades  = parse_entidades(args.enti_file)

    for recity in parse_recity(args.recity_file):
        if recity in municipios:
            lat, lon, provincia = municipios[recity]
            sys.stdout.write("{};{}:{},{}\n".format(provincia, recity, lat, lon))
        elif recity in entidades:
            lat, lon, provincia = entidades[recity]
            sys.stdout.write("{};{}:{},{}\n".format(provincia, recity, lat, lon))
        else:
            sys.stderr.write("?;{}:NA,NA\n".format(recity))

    return 0



def main():

    args = parse_args()
    get_coordinates(args)

    return 0



if __name__ == '__main__':
    main()
