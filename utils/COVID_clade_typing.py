#! /usr/bin/python3
# -*- coding: utf-8 -*-

'''
Script for typing SARS-CoV-2 genomes. 

It uses the tsv files derived from the IVAR pipeline
and a tab file with all the clade-defining mutations. 
To classify a sample as belonging to a clade, request 
at least one clade-defining mutation to be present. 
Genomic position and allelic change must match.
'''

#---------------------------------------------------------------
__author__      = "Álvaro Chiner-Oms"
__credits__     = ["Álvaro Chiner-Oms"]
__copyright__   = "Copyright 2020, SeqCOVID-Spain Consortium"
__email__       = "bioinfo.covid19@ibv.csic.es"
#---------------------------------------------------------------


import argparse
import pandas
import numpy


#################################################################
# Constant definitions
#################################################################
DEFAULT_CLADES_FILE='/data2/COVID19/reference_data/mutations_for_typing/clade_mutations.csv'
DEFAULT_MUTATS_FILE='/data2/COVID19/reference_data/mutations_for_typing/mutations_2.csv'

#################################################################


#################################################################
# Arguments parsing
#################################################################

def parse_args():
    '''
    Parse arguments given to script
    '''
        
    parser = argparse.ArgumentParser(
        description='Typing SARS-CoV-2 samples from a VCF file')

    parser.add_argument('-t', '--typ-table', 
        dest     = 'typ_table',
        required = False,
        default  = DEFAULT_CLADES_FILE,
        help     = 'Tab separated table with the mutations that define the clades of interest')

    parser.add_argument('-v', '--vcf_file', 
        dest='vcf_file',
        required = True,
        help='VCF file containing the variants called after running the pipeline. It could be .tsv (Illumina pipeline) or .variants.csv (nanopore pipeline)')

    parser.add_argument('-m', '--mut-table', 
        dest     ='mut_table', 
        required = False,
        default  = DEFAULT_MUTATS_FILE,
        help     ='Tab separated table with the mutations wanted to be surveillanced')

    args = parser.parse_args()
    
    return args


#################################################################
# Clade typing
#################################################################

def getClades_FromPositions_illumina(vcf_path, table = DEFAULT_CLADES_FILE):
    '''
    Function to extract information from the VCF file and type. 
    Only SNPs with a freq >= 80% and >= 30X are used for typing.
    A positions-allele match is required.
    '''

    # Read the VCF
    try:
        vcf_table = pandas.read_csv(vcf_path, sep='\t') 
    except: # pandas.errors.EmptyDataError: No columns to parse from file
        return 'NA'

    # Only positions having more than 30X
    depth_filter = vcf_table['TOTAL_DP'] >= 30

    # Only alleles above 80% freq
    freq_filter = vcf_table['ALT_FREQ'] >= 0.8

    # Make a copy of the rows of interest, to avoid warning
    vcf_table_filtered = pandas.DataFrame(vcf_table[freq_filter & depth_filter].copy()) 

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t'))

    # Resulting output
    result    = ''
    aux       = ''
    nclades   = 0
    separator = '&'

    # For each row
    for row in typing_table.itertuples():

        # If any of the TSV rows agrees with a
        # row of the list in POSITION and ALLELE 
        if not vcf_table_filtered[(vcf_table_filtered['POS'] == row.POSITION) & \
           (vcf_table_filtered['ALT'] == row.ALLELE)].empty:

            # Check that it has been detected by other mutation
            # (at least two mutations for typing a clade)
            # and that the typing is not duplicated 
            if (row.CLADE in aux and row.CLADE not in result) or (row.CLADE in ["SEC5", "SEC6", "SEC7"]): 
                result   = result + row.CLADE + separator
                nclades += 1            
            aux = aux + row.CLADE + separator

    # No clades found code
    if nclades == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result

def getClades_FromPositions_nanopore(vcf_path, table = DEFAULT_CLADES_FILE):
    '''
    Function to extract information from the VCF file and type. 
    All the SNPs that passed the Nextflow filters are taken into 
    account
    '''

    # Read the VCF
    try:
        vcf_table = pandas.read_csv(vcf_path, sep=',', comment='#', engine='python') #como separador ,
    except: # pandas.errors.EmptyDataError: No columns to parse from file
        return 'NA'

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t'))

    # Resulting output
    result    = ''
    aux       = ''
    nclades   = 0
    separator = '&'

    # For each row
    for row in typing_table.itertuples():

        # If any of the TSV rows agrees with a
        # row of the list in POSITION and ALLELE 
        if not vcf_table[(vcf_table['dna_var'] == (row.REF + str(row.POSITION) + row.ALLELE))].empty:

            # Check that it has been detected by other mutation
            # (at least two mutations for typing a clade)
            # and that the typing is not duplicated 
            if (row.CLADE in aux and row.CLADE not in result) or (row.CLADE in ["SEC5", "SEC6", "SEC7"]): 
                result   = result + row.CLADE + separator
                nclades += 1            
            aux = aux + row.CLADE + separator

    # No clades found code
    if nclades == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result

#################################################################
# Mutation scanning
#################################################################

def getMutations_FromPositions_illumina(vcf_path, table = DEFAULT_MUTATS_FILE):
    '''
    Function to extract information from the VCF file and look
    for mutations of interest. 
    Only SNPs with a freq >= 5% and >= 30X are used for typing.
    A positions-allele match is required.
    '''
    gene_equivalence = {
        'cds-QHD43415.1' : 'ORF1ab',
        'cds-QHD43416.1' : 'S',
        'cds-QHD43417.1' : 'ORF3a',
        'cds-QHD43418.1' : 'E',
        'cds-QHD43419.1' : 'M',
        'cds-QHD43420.1' : 'ORF6',
        'cds-QHD43421.1' : 'ORF7a',
        'cds-QHD43422.1' : 'ORF8',
        'cds-QHD43423.2' : 'N',
        'cds-QHI42199.1' : 'ORF10',
        'NaN' : 'NaN'
    }

    gene_start = {
        'ORF1ab' : 266,
        'S' : 21563,
        'ORF3a' : 25393,
        'E' : 26245,
        'M' : 26523,
        'ORF6' : 27202,
        'ORF7a' : 27394,
        'ORF8' : 27894,
        'N' : 28274,
        'ORF10' : 29558,
        'Nan' : 'nan'
    }

    # Read the VCF
    try:
        vcf_table = pandas.read_csv(vcf_path, sep='\t') 
    except: # pandas.errors.EmptyDataError: No columns to parse from file
        return 'NA'

    # Only positions having more than 30X
    depth_filter = vcf_table['TOTAL_DP'] >= 30 

    # Only alleles above 80% freq
    freq_filter = vcf_table['ALT_FREQ'] >= 0.05 

    # Make a copy of the rows of interest, to avoid warning
    vcf_table_filtered = pandas.DataFrame(vcf_table[freq_filter & depth_filter].copy())

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t')) 

    # Resulting output
    result = ''
    nmuts = 0
    separator = '&'

    # Check exception
    try:
        vcf_table_filtered['GFF_FEATURE'].replace(gene_equivalence, inplace=True) # change code for gene name
    except:
        return 'NA'

    # Initialize the codons list
    codons=list()

    for row in vcf_table_filtered.itertuples(): #obtain the codon number
        if not pandas.isna(row.GFF_FEATURE):
            cd = ((row.POS - gene_start[row.GFF_FEATURE]) // 3) + 1
            codons.append(str(cd))
        else:
            codons.append('nan')
    
    vcf_table_filtered['CODON_NUM'] = codons

    # For each row
    for row in typing_table.itertuples(): 

        # If any of the TSV rows agrees with a row
        # of the list in GFF_FEATURE,CODON,AA_REF and AA_ALT
        if not vcf_table_filtered[(vcf_table_filtered['CODON_NUM'] == row.CODON) & \
            (vcf_table_filtered['GFF_FEATURE'] == row.GENE) & \
            (vcf_table_filtered['REF_AA'] == row.AA_REF) & \
            (vcf_table_filtered['ALT_AA'] == row.AA_ALT)].empty: 
            # Get & round the value
            value = vcf_table_filtered[(vcf_table_filtered['CODON_NUM'] == row.CODON) & \
            (vcf_table_filtered['GFF_FEATURE'] == row.GENE) & \
            (vcf_table_filtered['REF_AA'] == row.AA_REF) & \
            (vcf_table_filtered['ALT_AA'] == row.AA_ALT)].iloc[0,10]
            value = round(value, 2)

            # Append the result
            result = result \
                + row.MUTATION \
                + '(' \
                + str(value) \
                + ')' + separator
            nmuts += 1


    # No mutations found code
    if nmuts == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result

def getMutations_FromPositions_nanopore(vcf_path, table = DEFAULT_MUTATS_FILE):
    '''
    Function to extract information from the VCF file and look
    for mutations of interest.
    All the SNPs that passed the Nextflow filters are taken into 
    account 
    '''
    # Read the VCF
    try:
        vcf_table = pandas.read_csv(vcf_path, sep=',', comment='#', engine='python') #como separador , 
    except: # pandas.errors.EmptyDataError: No columns to parse from file
        return 'NA'

    # Read the table
    typing_table = pandas.DataFrame(pandas.read_csv(table, sep='\t')) 

    # Resulting output
    result=''
    nmuts=0
    separator = '&'

    # For each row
    for row in typing_table.itertuples(): 

        # If any of the TSV rows agrees with a row
        # of the list in POSITION and ALLELE 
        if not vcf_table[(vcf_table['gene'] == row.GENE) & \
           (vcf_table['aa_var'] == (row.AA_REF + str(row.CODON) + row.AA_ALT))].empty: 

            # Get & round the value of the mutation frequency

            value = 'NA'

            # Append the result
            result = result \
                + row.MUTATION \
                + '(' \
                + value \
                + ')' + separator
            nmuts += 1


    # No mutations found code
    if nmuts == 0:
        return 'NA'

    # Remove last separator
    result = result [:-1]

    # Get the result
    return result
#################################################################
# Utils
#################################################################


def getCov_FromVcfPath(vcf_path):
    '''
    Remove the path folders and extensions for the given path
    '''

    # Initially, copy the path
    cov = vcf_path

    # Remove folders from path, keeping last element
    if cov.find("/") != -1:        
        cov = cov.rsplit("/",1)[1]
    
    # Remove de '.tsv' extension.
    if 'variants' in cov:
        cov = cov.replace('.variants.csv', '')
    else:
        cov = cov.replace('.tsv', '')

    # Return the COV (or file identifier)
    return cov



#################################################################
# Main functions
#################################################################

def main():
    '''
    Main entry point
    '''
    
    # Parse args
    args = parse_args()

    # Get the clade list
    if str(args.vcf_file).endswith('.csv'): #de momento así, pero tal vez se le puede poner un argumento al invocar el script y que no dependa del nombre del fichero
        clade_list = getClades_FromPositions_nanopore(args.vcf_file, args.typ_table)
    else:
        clade_list = getClades_FromPositions_illumina(args.vcf_file, args.typ_table)

    # Get the mutations list
    if str(args.vcf_file).endswith('.csv'):
        mutations_list = getMutations_FromPositions_nanopore(args.vcf_file, args.mut_table)
    else:
        mutations_list = getMutations_FromPositions_illumina(args.vcf_file, args.mut_table)

    # Get the corresponing COV (of file id)
    cov = getCov_FromVcfPath(args.vcf_file)
    
    # Print results
    print(cov, '\t', clade_list, '\t', mutations_list)
    
    
if __name__ == "__main__":
    main()


