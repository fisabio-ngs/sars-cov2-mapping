# NextSpain: Spanish SARS-CoV-2 sequencing consortium

This repository contains everything related to the bioinformatic analysis of
the data produced by the Spanish SARS-CoV-2 sequencing consortium. For further
information about this project, please visit
[http://seqcovid.csic.es/](http://seqcovid.csic.es/).

### Project summary
This is a Spanish, nationwide project in which samples from COVID19-positive
patients are sequenced to obtain the whole-genome of the infecting virus.
Although data will be used for other research purposes, the primary goal of the
project is to study the transmission dynamics of COVID19 in Spain since the pandemic
started.

### General Setup
The whole genome of the virus is amplified by PCR using the ARTIC primer scheme
and samples are then sequenced using Illumina platforms and Nextera Flex
chemistry. Therefore, the analysis pipeline that is described here applies
to this particular setup. Given that some samples are sequenced using nanopore
techonolgy, an analysis pipeline for nanopore data is also described.


## Analysis of Illumina data
The pipeline operation, input and output files are intended to fit the SOPs of
the project and our laboratory, but should work for independent analyses if
everything is installed and properly set.

### Requisites
A working conda environment must be set that has available the following packages:
* [FastQC](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/)
* [multiqc](https://multiqc.info/)
* [fastp](https://github.com/OpenGene/fastp)
* [bwa](http://bio-bwa.sourceforge.net/)
* [samtools](http://www.htslib.org/)
* [iVar](https://github.com/andersen-lab/ivar/blob/master/docs/MANUAL.md)

For the QC reports we run ad-hoc python scripts that need [numpy](https://numpy.org/) 
to be installed.

The file `pipeline_parameters.config` must be edited to set:
* The path to the conda environment
* The path to reference genome and annotation
* The parameters of the analysis for the different steps

### Running the script
Input:
* A semicolon-separated table containing the name of the samples and their respective fastq files (3 columns)
* An input folder where the fastq files are stored
* An output folder where the analysis files will be written
* A BED6 file with the primer scheme used for amplification (ARTIC in this case)
* A sequencing RUN identifier (any string can be provided to identify the analysis)

### Analysis overview
The workflow for illumina data is based on [iVar](https://github.com/andersen-lab/ivar/blob/master/docs/MANUAL.md)
and consists of the following steps:

* Quality trimming
* Mapping of reads to MN908947.3 reference genome
* Primer trimming
* Consensus genome generation
* Variant calling
* Basic QC summaries
