#!/usr/bin/bash
#################################################################################
## SeqCOVID-Illumina-pipeline.sh 
## 
## Description:
##    Analyze Sars-Cov-2 data within the frame of the 
##    SeqCOVID-Spain consortium: Sars-Cov-2 sequencig project
##
##    For any question please visit:
##    https://gitlab.com/fisabio-ngs/sars-cov2-mapping
##
##
## Unidad de Análisis Computacional COVID19 / Unidad de Genómica de Tuberculosis
## Instituto de Biomedicina de Valencia
## Calle Jaume Roig, 11 (46010)
## València, España
##
## Fundación para el Fomento de la Investigación Sanitaria y 
## Biomédica de la Comunitat Valenciana (FISABIO)
## Servicio de Secuenciación y Bioinformática
## Av. Catalunya, 21 (46020)
## València, España
##
## __author__    = Galo A. Goig
## __author__    = Giuseppe D'Auria
## __author__    = Santiago Jiménez-Serrano
## __author__    = Álvaro Chiner-Oms
## __copyright__ = Copyright 2020, SeqCOVID-Spain Consortium
## __email__     = bioinfo.covid19@ibv.csic.es
## __date__      = 2021 April
## __version__   = 1.0.2
##
################################################################################

################################################################################
#                                                           FUNCTION DEFINITIONS
################################################################################
# First block: Function definitions that will run during the analysis

################################################################################
# Function of usage message and help
################################################################################
usage () 
{
	echo -e $1 >&2
	echo "$(basename $0)"
	echo "Usage:"
	echo "  -h                 Display this help message"
	echo "  -m  MASTER_TABLE   Table with sample names and respective barcodes"
	echo "  -f  FASTQFOLDER    path to fastq_pass folder as obtained from guppy"
	echo "                     (Where this script will read data from)"
	echo "  -r  RUNID"
	echo "  -o  OUTFOLDER      Folder where the analysis will be run and where"
	echo "                     the results will be stored. OUTFOLDER should match the RUN ID."
	echo "  -t  THREADS        Number of threads to be used by multithreading-compatible software"
	echo ""
	exit -1
}

################################################################################
# Log functions
################################################################################
# Print and log messages in LOG_FILE
log_string() 
{
    echo -e "$@" | tee -a ${LOG_FILE}
}

# Run a command and log any message printed to stdout in LOG_FILE
log_command() 
{
    echo -e "$@" |  tee -a ${LOG_FILE}
    echo "$@" | bash 2>>${LOG_FILE} | tee -a ${LOG_FILE}
}

################################################################################
# FastQC Quality 
################################################################################
fastqc() 
{
	# SAMPLE R1 and R2 are passed to fastp_trim as read below from master_table
	# with a while loop
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Quality assessment using FastQC"
	log_string "-----------------------------------------------------------------"
	log_command "fastqc ${INFASTQFOLDER_REAL}/*fastq.gz -o reports/fastqc --threads ${THREADS} --quiet "
	log_string ""
}

################################################################################
# Kraken Filtering
################################################################################
kraken_filter()
{
	echo 'fastqfile ==> ' $fastqfile

	# Removal of reads classified as Homo Sapiens (taxid: 9606) by Kraken.

	# Input file paths (fastq.gz)
	INFQ=${folder_fastq_pass}'/'${BARCODE}'/'${1}

	# Get the base output filename
	basename=$(echo "${1}" | sed 's/.fastq.gz//g')
	basename=${folder_fastq_filt}'/'${BARCODE}'/'${basename}

	# Output file paths
	outfastq=${basename}'.humanfilt.fastq'
	outkraken=${basename}'.kraken'	
	outkraken_hids=${outkraken}'.human.ids'

	# Debug
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Removal of human reads with kraken: ${BARCODE}/${1}'     "
	log_string "-----------------------------------------------------------------"

	# First taxonomically classify the reads
	log_command "/data/Software/Kraken/kraken "  \
		" --db ${KRAKEN_DB} "      \
		" --fastq-input "          \
		" --output ${outkraken}"   \
		" --threads ${THREADS} "   \
		" --gzip-compressed ${INFQ} "
	

	# Get the identifiers of reads that belong to human (taxid: 9606)
	awk '$3 == "9606" { print $2 }' ${outkraken} > ${outkraken_hids}
	

	# Pick reads from a fastq given a list of readids using ad-hoc script
	log_command "${SCRIPT_DIR}/../utils/Script-fasta-pick-readlist.py -in ${INFQ}" \
                " -r ${outkraken_hids} -a discard " \
                " -o ${outfastq} -f fastq -c"	

	# Gzip fastq file
	log_command "gzip -f ${outfastq}"
	
	#remove intermediate files
	log_command "rm ${outkraken_hids}"
}


kraken_filter_parallel()
{
	#########################################################
	# Sequential way of do this #############################
	# ls *.fastq.gz | while read fastqfile
	# do 
 	# 	# Perform the human filtering for such sample
	# 	kraken_filter ${fastqfile}
	# 	echo ${fastqfile}
	# done
	#########################################################

	# Debug
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "   Removal of human reads with kraken: ${BARCODE}/${COV}         "
	log_string "-----------------------------------------------------------------"

	# Environment initialization ####################################
	#################################################################

	# Original barcode bolder
	BARCODE_FOLDER=${folder_fastq_pass}'/'${BARCODE}

	# Create the filtered fastq/barcodeXX folder
	mkdir ${folder_fastq_filt}'/'${BARCODE}

	# Change to original barcode folder
	cd ${BARCODE_FOLDER}

	# List of pids to wait
	pids=()

	# Run a parallel process for each fastq.gz file
	for fastqfile in $(ls *.fastq.gz)
	do 
 		# Perform the human filtering for such sample
		COVID-kraken-filter-nanopore.sh \
			${folder_fastq_pass}  \
			${folder_fastq_filt}  \
			${BARCODE}            \
			${fastqfile}          \
			${KRAKEN_DB}          \
			${THREADS} &

		# Add the pid to the waiting list
		pids+=($!)

	done

	# Wait until all the child-processes end
	for pid in ${pids[@]}
	do
		wait $pid
		#echo 'pid... finished ' $pid
	done
	
}


gzip_fastq_parallel()
{
	# Move the fastq file to the final folder
	log_command "mv ${guppyPlex}/* ${folder_fastq}/"
	log_command "rename .fastq .humanfilt.fastq ${folder_fastq}/*"


	# List of pids to wait
	pids=()

	log_string "-- Gzipping fastqs in ${folder_fastq} --"


	for fq in $(ls ${folder_fastq}/*.fastq)
	do
		log_string "gziping... ${fq}"
		gzip ${fq} &

	 	# Add the pid to the waiting list
		pids+=($!)
	done

	# Wait until all the child-processes end
	for pid in ${pids[@]}
	do
		wait $pid
	done

	log_string "-- [DONE] Gzipping fastqs--"

}

################################################################################
# Generate Kraken reports based on .kraken files
################################################################################
kraken_report()
{	
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "   Generating kraken report: ${BARCODE}/${COV}                   "
	log_string "-----------------------------------------------------------------"



	#========================= ORIGINAL illumina
	#log_command "/data/Software/Kraken/kraken-report --db ${KRAKEN_DB} ${FF_REAL}/${SAMPLE}.kraken > reports/${SAMPLE}.report"
	# I will take advantage of ThePipeline contaminants function to extract ready-to-plot data
	#log_command "/data/ThePipeline/ThePipeline contaminants -p reports/${SAMPLE}"
	#log_command "gzip -f ${FF_REAL}/${SAMPLE}.kraken"
	#=========================

	# Input file paths
	file_kraken=${folder_fastq_filt}'/'${BARCODE}'/'${COV}'.kraken'

	# Outut file path
	base_report=${OUTFOLDER_REAL}'/reports/'${COV}	

	# First, put together all the .kraken files
	log_command "cat ${folder_fastq_filt}/${BARCODE}/*.kraken > ${file_kraken}"
	
	# Output file paths
	out_report=${base_report}'.report'

	# Create the report
	log_command "/data/Software/Kraken/kraken-report --db ${KRAKEN_DB} ${file_kraken} > ${out_report}"

	# Call ThePipeline contaminants function to extract ready-to-plot data
	log_command "/data/ThePipeline/ThePipeline contaminants -p ${base_report}"
	log_command "gzip -f ${file_kraken}"
	log_command "rm ${folder_fastq_filt}/${BARCODE}/*.kraken"
	log_command "mv ${file_kraken}.gz ${folder_fastq}"

	# Clean some reports (remove long paths and replace for the cov code)	
	sed -i "s#${base_report}#${COV}#" ${base_report}'.species.contaminants'
	sed -i "s#${base_report}#${COV}#" ${base_report}'.genus.contaminants'
}

################################################################################
# Run nextflow container with the artic_nf pipeline for nanopore
################################################################################
nextflow_pipeline()
{	
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Runing the Nextflow CLIMB Nanopore pipeline in all samples                      "
	log_string "-----------------------------------------------------------------"

	# Run the medaka pipeline

	cache_folder='/home/sjimenez/cache-nf_nextflow/'
	nf_typing_folder='/data2/COVID19/software/connor-lab/ncov2019-artic-nf/typing'
	gff=${nf_typing_folder}'/MN908947.3.gff'
	yaml=${nf_typing_folder}'/SARS-CoV-2.types.yaml'
	
	log_command "nextflow ${ARTIC_NF_FOLDER} "               \
				" --medaka "                                 \
				" --prefix ${RUNID} "                        \
				" --basecalled_fastq ${folder_fastq_filt} "  \
				" --gff  ${gff} "                            \
				" --yaml ${yaml} "                           \
				" --outdir ${OUTFOLDER_REAL} "               \
				" --cache ${cache_folder} "                  \
				" -profile conda "

}

################################################################################
# Statistics and coverage reports
################################################################################
report () 
{
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "  Flagstat statistics and coverage calculation: ${COV}           "
	log_string "-----------------------------------------------------------------"


	# El fichero de entrada  en illumina es $COV.trim.sort.bam
	# El fichero que tenemos en nanopore es $COV.mapped.sorted.bam
	bamfile=${OUTFOLDER_REAL}'/'${COV}'.mapped.sorted.bam'

	# Output files
	bamstatsfile=${OUTFOLDER_REAL}'/reports/'${COV}'.bamstats'
	depthfile=${OUTFOLDER_REAL}'/reports/'${COV}'.depth'
	wdepthfile=${OUTFOLDER_REAL}'/reports/'${COV}'.wdepth'
	adepthfile=${OUTFOLDER_REAL}'/reports/'${COV}'.adepth'


	log_string ""
	log_command "samtools flagstat ${bamfile} > ${bamstatsfile}"
	
	log_string ""
	log_command "samtools depth -aa --reference ${REF_GENOME} ${bamfile} > ${depthfile}"
	
	log_string ""
	log_command "python $SCRIPT_DIR/../utils/COVID-window-coverage.py" \
                "-i ${depthfile} -p ${PRIMER}"              \
                "-w ${WIND_DEPTH} -H -s ${COV} > ${wdepthfile}"
	
	log_string ""
	log_command "python $SCRIPT_DIR/../utils/COVID-amplicon-depth.py" \
                "-i ${depthfile}  -p ${PRIMER}"             \
                "-o ${adepthfile} -s ${COV} -r ${RUNID}"
	log_string ""
}


################################################################################
# Write QC_Summary
################################################################################
summary () 
{
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string " Writting QC Summary"
	log_string "-----------------------------------------------------------------"

	# 1 - Generates the QC report (../reports/QC_summary.'${RUNID}'.csv')
	log_string ""
	log_string "--- Get run analysis summary (only COVs samples)"
	log_command "${SCRIPT_DIR}/../utils/COVID-analysis-summary-nanopore.py " \
		" -m ${MTABLE_REAL} "                   \
		" --run-id ${RUNID} "                   \
		" --run-directory ${OUTFOLDER_REAL}"    \
		" -o ${OUTFOLDER_REAL}/reports/QC_summary.${RUNID}.csv"
	log_string ""

	# 2 - Generates the 'all' QC report (../reports/QC_summary.'${RUNID}'.all.csv')		
	log_string ""
	log_string "--- Get run analysis summary (all samples)"
	log_command "${SCRIPT_DIR}/../utils/COVID-analysis-summary-nanopore.py" \
		" -m ${MTABLE_REAL} "                   \
		" --run-id ${RUNID} "                   \
		" --run-directory ${OUTFOLDER_REAL}"    \
		" -all yes "                            \
		" -o ${OUTFOLDER_REAL}/reports/QC_summary.${RUNID}.all.csv"
	log_string ""

	# Get the seq place
	qc_file=${OUTFOLDER_REAL}'/reports/QC_summary.'${RUNID}'.csv'
	seqplace=$(tail -n +2 ${qc_file} | cut -f3 -d';' | uniq)

	# Call some updating runid scripts
	# 3 - Generates long QC report (../reports/QC_summary.'${RUNID}'.long.csv')
	log_string ""
	log_string "--- Get run analysis summary (long qc summary, only COVs samples) "	
  	log_string "--- COVID-update-QC_summaries_LONG.sh"
	log_command "${SCRIPT_DIR}/../utils/pipeline-data-update/COVID-update-QC_summaries_LONG.sh ${seqplace} ${RUNID} nanopore"
}

################################################################################
# Assign lineages with PANGOLIN
################################################################################
assign_lineages () 
{
	mkdir ${OUTFOLDER_REAL}/pangolin
	log_string ""
	log_string "-----------------------------------------------------------------"
	log_string "		Lineage assignment with PANGOLIN"
	log_string "-----------------------------------------------------------------"
  	log_string "--- Creating a multifasta for pangolin"
	log_command "cat ${OUTFOLDER_REAL}/consensus/*.fa > ${OUTFOLDER_REAL}/consensus/consensus.${RUNID}.fna"
	log_command "pangolin ${OUTFOLDER_REAL}/consensus/consensus.${RUNID}.fna" \
	 	" -o ${OUTFOLDER_REAL}/pangolin/ --outfile lineages.${RUNID}.csv"     \
		" -t ${THREADS} --max-ambig 0.3"		
	log_string ""
	log_command "rm ${OUTFOLDER_REAL}/consensus/consensus.${RUNID}.fna"
}

################################################################################
# Create Metadata
################################################################################
# Create metadata tables ready for GISAID, MICROREACT and ENA submission
create_metadata () 
{
	log_string "-----------------------------------------------------------------"
	log_string "		Generating metadata tables "
	log_string "-----------------------------------------------------------------"	

	# Auxiliar variables
	metadata_folder=${OUTFOLDER_REAL}'/metadata'
	dbupdate_file=${metadata_folder}'/DB_UPDATE_'${RUNID}'.csv'

	# Call the analisys-metadata script
	log_string ""
	log_command "${SCRIPT_DIR}/../utils/COVID-analysis-metadata-nanopore.py " \
		" -m ${MTABLE_REAL} " \
		" --run-id ${RUNID} " \
		" --run-directory ${OUTFOLDER_REAL} " \
		" -o ${metadata_folder} "
	log_string ""


	# Check file dbupdate file was generated
	if [ ! -f ${dbupdate_file} ]; then
		log_string "[ERROR]: DB Updating file was not generated: ${dbupdate_file}"
		return
	fi

	# Get the seq place
	seqplace=$(tail -n +2 ${dbupdate_file} | cut -f7 -d',' | uniq)

	# Call some updating runid scripts
	# 1 - Generates clades report (../variants/Typing_Report.'${RUNID}'.txt')		
	log_string "-----------------------------------------------------------------"
	log_string "		Clades & mutations report generation "
	log_string "-----------------------------------------------------------------"
  	log_string "--- COVID-update-clades.sh"
	log_command "${SCRIPT_DIR}/../utils/pipeline-data-update/COVID-update-clades.sh ${seqplace} ${RUNID} nanopore"
}

################################################################################
# Concatenated Multifasta
################################################################################
# Create a multifasta with genomes that pass QC threshold of 90% coverage and
#  ready for manual inspection for GISAID upload and
# lineae assinment with pangolin
multifasta_consensus () 
{
	echo "--- Creating HQ multifasta"
	# Read which genomes pass QC 90% from summary
	tail -n +2 ${OUTFOLDER_REAL}/reports/QC_summary.${RUNID}.csv  |
	awk -v FS=';' '$6 >= 0.9' | cut -f 1 -d ';' | 
	while read COV
	do
		faf=${OUTFOLDER_REAL}/consensus/${COV}.fa
		sed -e "s/Consensus_//" ${faf} -e "s/_threshold_${MINFREQ_CONS}_quality_${MINQUAL_CONS}//" > ${faf}.tmp
		mv ${faf}.tmp ${faf}
		cat ${faf}
	done > ${OUTFOLDER_REAL}/consensus/consensus.${RUNID}.fasta
	
	# Create a consensus with GISAID headers read from metadata
	echo "--- Creating GISAID multifasta with SARS-Cov-2 reference genome"

	# Avoid print reference
	#cat /data2/COVID19/reference_data/Sars-Cov-2_REFERENCE.fasta > ${OUTFOLDER_REAL}/consensus/GISAID_consensus.${RUNID}.fasta
	tail -n +2 ${OUTFOLDER_REAL}/metadata/GISAID_${RUNID}_metadata.csv |
	cut -f3,26 -d'@' | sed 's/@/ /' | 
	while read GISAID_ID COV_ID
	do
		cat ${OUTFOLDER_REAL}/consensus/${COV_ID}.fa | sed "s|${COV_ID}|${GISAID_ID}|"
	done >> ${OUTFOLDER_REAL}/consensus/GISAID_consensus.${RUNID}.fasta

}


################################################################################
# Utilities
################################################################################

function runid_barcode_2_cov()
{
	log_command "sed -i 's/${RUNID}_//g'        $1"
	log_command "sed -i 's/${BARCODE}/${COV}/g' $1"
}


################################################################################
#                      **********************************
#                                 MAIN SCRIPT
#                      **********************************
################################################################################
# Second block:
#   Arguments provided by user are read, validated and
#   all the pipeline is run with respective function calls

COMMAND_CALL="$0 $*"

################################################################################
# Read input parameters
################################################################################
while getopts ":hm:f:r:o:s:t:" option
do
	case "${option}" in
		h)
			usage;;
		m) MTABLE=${OPTARG};;
		f) INFASTQFOLDER=${OPTARG};;
		o) OUTFOLDER=${OPTARG};;
		r) RUNID=${OPTARG};;
		s) STEPS=${OPTARG};;
		t) THREADS=${OPTARG};;
		\?)
			usage;;
		:)
			echo "Invalid option: $OPTARG requires an argument" >&2;;
	esac
done
shift "$(($OPTIND -1))"

################################################################################
# Validate input parameters
# IF Valid, change relative paths TO ABSOLUTE PATHS
################################################################################

# Master table
if [ -z $MTABLE ]; then
	usage "\n[ERROR] No MASTER_TABLE table provided!\n"	
else
	MTABLE_REAL=$(readlink -f ${MTABLE})
	echo "Input master table: ${MTABLE_REAL}"
fi

# FastQ folder
if [ -z $INFASTQFOLDER ]; then	
	usage "\n[ERROR] No FASTQ folder provided!\n"	
else
	INFASTQFOLDER_REAL=$(readlink -f ${INFASTQFOLDER})
	echo "Input fastq folder: ${INFASTQFOLDER_REAL}/"
fi

# Run Id
if [ -z ${RUNID} ]; then	
	usage "\n[ERROR] No RUNID provided\n"	
fi

# Output Folder
if [ -z $OUTFOLDER ]; then	
	usage "\n[ERROR] No OUTFOLDER provided\n"	
else
	OUTFOLDER_REAL=$(readlink -f ${OUTFOLDER})
	echo "Output results folder: ${OUTFOLDER_REAL}/"
fi

# Running Threads number
if [ -z $THREADS ]; then
  THREADS=1
else
	case $THREADS in
		[0-9]*) echo "Running $THREADS threads";;
	  *)
		usage "\n[ERROR] Argument provided for -t (threads) must be numeric\n" ;;
	esac
fi


################################################################################
# Get relative paths, date and pipeline parameters
################################################################################

# Get absolute path of the illuminaMapping.sh script
SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Init pipeline parameters from pipeline_parameters.config
source $SCRIPT_DIR/pipeline_nanopore_parameters.config

# Init conda environment
source activate ${CONDA_ENV}
if [ $? != 0 ]; then # if there is any problem init conda #!/usr/bin/env
	echo ""
	echo "conda env could not be activated. Please check that a proper conda"\
		"environment is set and that the path to that environment is set in"\
		"variable CONDA_ENV in the configuration file 'pipeline_parameters.config'"
	echo ""
	exit 1
fi

# NOTE THAT AT THIS POINT WE CHANGE TO $OUTFOLDER DIRECTORY AND EVERYTHING
# IS EXECUTED FROM THERE
# Check if OUTFOLDER, was created manually by user with RUNID name previously
# Otherwise, try to create it 
if [[ ! -d "$OUTFOLDER_REAL" ]]; then	
	echo "-----------------------------------------------------------------"
	echo "	Creating ${OUTFOLDER} directory"
	echo "-----------------------------------------------------------------"
	mkdir ${OUTFOLDER}
	OUTFOLDER_REAL=$(readlink -f ${OUTFOLDER})    
fi



################################################################################
#                                                          Create directory tree
################################################################################
mkdir ${OUTFOLDER_REAL}/consensus
mkdir ${OUTFOLDER_REAL}/variants
mkdir ${OUTFOLDER_REAL}/logfolder
mkdir ${OUTFOLDER_REAL}/reports
mkdir ${OUTFOLDER_REAL}/metadata


################################################################################
#                                                        Initialize the LOG_FILE
################################################################################

# Get date and define LOG_FILE name
DATE=$(date +"%y%m%d%H%M%S")
LOG_FILE=${OUTFOLDER_REAL}'/logfolder/RUN.'${RUNID}'.log'

# Log
log_string "============================================="
log_string "Nanopore mapping pipeline"
log_string "please visit: https://github.com/fisabio-ngs/" #update the repository
log_string "============================================="
log_string "Starting analysis at: $DATE"
log_string "=============== Config viarables ==============="
log_string "CONDA_ENV=${CONDA_ENV}"
log_string "COMMAND_CALL=$COMMAND_CALL"
log_string "NEXTFLOW_CONTAINER=$ARTIC_NF_FOLDER"
log_string "----------------------------------------------"
log_string "RUNID=${RUNID}"
log_string "FASTQFOLDER=$INFASTQFOLDER_REAL"
log_string "OUTFOLDER=$OUTFOLDER_REAL"
log_string "REFERENCE=$REF_GENOME"
log_string "PRIMER=$PRIMER"
log_string ""
log_string "=============== DEPTH CALCULATION VARIABLES ==============="
log_string "WIND_DEPTH=$WIND_DEPTH      # Window size to compute mean and median depths"
log_string "MIN_DEPTH=$MIN_DEPTH       # Min DEPTH quality threshold (mean)"
log_string "SAMPLES ARRAY=${Samples[@]}"
log_string "Steps="${Steps[@]}
log_string "============================================="
log_string ""


################################################################################
#                                         STORE VARIABLES IN RUN.RUNID.variables
################################################################################
VAR_LOG=${OUTFOLDER_REAL}'/logfolder/RUN.'${RUNID}'.variables'
echo "RUNID=${RUNID}"                 >  ${VAR_LOG}
echo "OUTFOLDER_REAL=$OUTFOLDER_REAL" >> ${VAR_LOG}
echo "MTABLE_REAL=$MTABLE_REAL"       >> ${VAR_LOG}
echo "MINFREQ_CONS=$MINFREQ_CONS"     >> ${VAR_LOG}
echo "MINQUAL_CONS=$MINQUAL_CONS"     >> ${VAR_LOG}


################################################################################
#                     Read input data from MASTER_TABLE and execute the pipeline
################################################################################

# Cat kraken files to cache to load the DB:
log_string "-----------------------------------------------------------------"
log_string "		Backup of Master Table and removal of '_rep'"
log_string "-----------------------------------------------------------------"
log_command "cp ${MTABLE_REAL} ${MTABLE_REAL}.backup"
log_command "sed -i 's/_rep//g' ${MTABLE_REAL}"

echo "Loading Kraken Database... this process will take some minutes"
cat ${KRAKEN_DB}database.* > /dev/null


# Base folders (input folder = fastq_pass)
folder_fastq=${INFASTQFOLDER_REAL}'/fastq'
folder_fastq_pass=${INFASTQFOLDER_REAL}'/fastq_pass'
folder_fastq_filt=${INFASTQFOLDER_REAL}'/fastq_pass_humanfiltered'


# Kraken filtering
cd ${INFASTQFOLDER_REAL}
mkdir ${folder_fastq}
mkdir ${folder_fastq_filt}



# Kraken filtering 
cat ${MTABLE_REAL} | grep -v 'SampleID' | awk -F ',' '{print $1,$2}' | 
while read COV BARCODE
do

	# Avoid final empty line
	if [ -z ${COV} ]; then
		continue
	fi	

	# Filter in parallel the fastq.gz files for this barcode
	kraken_filter_parallel
	
	# Create the contaminants reports(sars-cov2, homo-sapiens, ...)
	kraken_report ${BARCODE} ${COV}

	# Move to the original fastq pass folder
	cd ${folder_fastq_pass}
done


cd ${INFASTQFOLDER_REAL}


#Medaka magic
nextflow_pipeline 

# Abbrv.
guppyPlex=${OUTFOLDER_REAL}'/articNcovNanopore_sequenceAnalysisMedaka_articGuppyPlex'
amedaka=${OUTFOLDER_REAL}'/articNcovNanopore_sequenceAnalysisMedaka_articMinIONMedaka'
articgtv=${OUTFOLDER_REAL}'/articNcovNanopore_Genotyping_typeVariants'

#Rename barcodes to COV names, fasta to .fa, change fasta's headers, decompress VCF files, change fastq folder name
log_string "-----------------------------"
log_string "  Renaming FILES ==>>>       "
log_string "-----------------------------"

qc_csv=${OUTFOLDER_REAL}'/'${RUNID}'.qc.csv'
ty_csv=${OUTFOLDER_REAL}'/'${RUNID}'.typing_summary.csv'
va_csv=${OUTFOLDER_REAL}'/'${RUNID}'.variant_summary.csv'


cat ${MTABLE_REAL} | grep -v 'SampleID' | awk -F ',' '{print $1,$2}' | 
while read COV BARCODE
do 

	# Avoid final empty line
	if [ -z ${COV} ]; then
		continue
	fi

	log_string "-----------------------------"
	log_string "  Renaming ${BARCODE}/${COV} "
	log_string "-----------------------------"

	rid_barcode=${RUNID}'_'${BARCODE}

	log_command "rename ${rid_barcode} ${COV} ${guppyPlex}/*"
	log_command "rename ${rid_barcode} ${COV} ${OUTFOLDER_REAL}/qc_plots/*"	
	log_command "rename ${rid_barcode} ${COV} ${amedaka}/*"
	log_command "rename .consensus.fasta .fa ${amedaka}/*.fasta"
	log_command "rename ${rid_barcode} ${COV} ${articgtv}/typing/*.*"
	log_command "rename ${rid_barcode} ${COV} ${articgtv}/variants/*.*"
	log_command "rename ${rid_barcode} ${COV} ${articgtv}/vcf/*.*"
	

	
	log_command "sed -i 's/>.*/>${COV}/' ${amedaka}/${COV}.fa"

	# Replace the runid_barcode strings in the csv files
	runid_barcode_2_cov ${qc_csv}
	runid_barcode_2_cov ${ty_csv}
	runid_barcode_2_cov ${va_csv}
	runid_barcode_2_cov ${articgtv}'/typing/'${COV}'.typing.csv'
	runid_barcode_2_cov ${articgtv}'/variants/'${COV}'.variants.csv'


	log_command "mv ${OUTFOLDER_REAL}/qc_pass_climb_upload/${RUNID}/${rid_barcode}/*.bam ${OUTFOLDER_REAL}/"
	log_command "rename ${rid_barcode} ${COV} ${OUTFOLDER_REAL}/*.bam"

	log_string ""
	log_string ""
done


log_string "-----------------------------"
log_string "  Renaming FILES FINAL ==>>> "
log_string "-----------------------------"


# Gzip in parallel the fastq files
gzip_fastq_parallel

# Move everything to their corresponding folders and generate *.bai
log_command "mv ${amedaka}/*.fa ${OUTFOLDER_REAL}/consensus/"
log_command "mv ${amedaka}/*.pass.vcf.gz ${OUTFOLDER_REAL}/variants/"
log_command "gzip -d ${OUTFOLDER_REAL}/variants/*.pass.vcf.gz"
log_command "mv ${qc_csv} ${OUTFOLDER_REAL}/reports/${RUNID}.medaka.qc.csv"
log_command "mv ${ty_csv} ${OUTFOLDER_REAL}/reports/${RUNID}.medaka.typing_summary.csv"
log_command "mv ${va_csv} ${OUTFOLDER_REAL}/reports/${RUNID}.medaka.variant_summary.csv"
log_command "ls ${OUTFOLDER_REAL}/*.bam | xargs -I file -P 10 bash -c 'samtools index file'"



# Create report for each sample
cat ${MTABLE_REAL} | grep -v 'SampleID' | awk -F ',' '{print $1}' | 
while read COV
do
	# Avoid final empty line
	if [ -z ${COV} ]; then
		continue
	fi

	# Crea los ficheros de los reports		
	report ${COV}
done

# Deactivate Conda environment
source deactivate ${CONDA_ENV}


################################################################################
#                                               LINEAGE ASSIGNMENT WITH PANGOLIN
################################################################################
source activate ${PANG_ENV}
assign_lineages
source deactivate ${PANG_ENV}


################################################################################
#                                                         CREATE RESULTS SUMMARY
################################################################################
summary


################################################################################
#                                                          FINAL QC WITH MULTIQC
################################################################################
source activate ${CONDA_ENV}
log_string ""
log_string "-----------------------------------------------------------------"
log_string "		Run QC assessment with MultiQC"
log_string "-----------------------------------------------------------------"
multiqc ${OUTFOLDER_REAL} --filename multiqc.${RUNID}.flat.html --flat --outdir ${OUTFOLDER_REAL}/reports
multiqc ${OUTFOLDER_REAL} --filename multiqc.${RUNID}.interactive.html --interactive --outdir ${OUTFOLDER_REAL}/reports


################################################################################
#                              CREATE METADATA FILES AND MULTIFASTA GISAID-READY
################################################################################
log_string "-----------------------------------------------------------------"
log_string "		Creating metadata files"
log_string "-----------------------------------------------------------------"
create_metadata
log_string ""
log_string "--- Creating multifasta consensus"
multifasta_consensus


################################################################################
#                                           CLEAN INTERMEDIATE FOLDERS AND FILES
################################################################################
log_command "rm -rf ${folder_fastq_filt} "
log_command "rm -rf ${INFASTQFOLDER_REAL}/work/"
log_command "rm -rf ${OUTFOLDER_REAL}/articNcovNanopore_sequenceAnalysisMedaka_articDownloadScheme/"
log_command "rm -rf ${OUTFOLDER_REAL}/qc_pass_climb_upload/"
log_command "rm -rf ${guppyPlex}"

################################################################################
#                                                                  FINAL MESSAGE
################################################################################
log_string ""
log_string "Analysis completed at: ${DATE}"
